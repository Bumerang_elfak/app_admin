#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include <QTimer>
#include <QMessageBox>

#include "Engine/komunikacija_sa_serverom.h"
#include "Engine/Osobe/profesor.h"
#include "Engine/PPOK/predmet.h"
#include "Engine/Komanda/komanda_logovanje.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_logovanje.h"
#include "Engine/Komanda/Komanda.h"
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"
#include "Engine/PPOK/predmet.h"
#include "Engine/Osobe/osobe.h"
#include "Engine/Osobe/admin.h"
#include "Engine/PPOK/oblast.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_predmet.h"

class Controller : public QObject
{
    Q_OBJECT
public:

    explicit Controller(QObject *parent = 0);
    void postavi_komunikaciju()
    {
        this->komunikacija= Komunikacija_sa_Serverom::vrati_komunikaciju();
        connect(komunikacija,SIGNAL(stigo_odg(Odgovor_na_Komandu*)),this,SLOT(primi_odgovor_na_komandu(Odgovor_na_Komandu*)));
    }

    void postavi_administratora(int id,QString ime, QString prez, QString email, QString user, QString pass)
    {
        admin=new Admin(id,ime,prez,email,user,pass);
    }
    void komanda_vrati_predmete()
    {
        id_komanda_predmeti=1;
        komandaupit_predmet *kpredmet=new komandaupit_predmet();//svi predmeti
        komunikacija->salji_komandu(kpredmet);
    }
    void komanda_vrati_profesore()
    {
        id_komanda_profesori=1;
        komandaupit_profesor* kprofesor=new komandaupit_profesor();//svi profesori
        komunikacija->salji_komandu(kprofesor);
    }
    QList<Predmet> vrati_predmete() {return this->predmeti;}
    QList<Profesor> vrati_profesore() {return this->profesori;}
    void dodaj_predmet(QString ime_predmeta, QString email_profesora);
    void dodaj_predmet_2();
    void brisi_predmet(QString ime_predmeta);
    void brisi_predmet_2();
    QList<Predmet> komanda_vrati_predmete_profesora(QString email_profesora);
    void logovanje_administratora(QString u, QString p);
    Profesor komanda_vrati_profesora_predmeta(QString ime_predmeta);
    QString vrati_podatke(int id);


    int vrati_id_profesora(QString profesor);
    int vrati_id_predmeta(QString predmet);
    void dodaj_profesora(QString ime, QString prezime, QString email, QString us, QString pass);
    void brisi_profesora(QString email_profesora);
    void brisi_profesora_2();


private:

    QList<Predmet> predmeti;
    QList<Profesor> profesori;
    QList<Profesor> profesori_bez_predmeta;
    QList<Profesor> logovani_profesori;
    Admin* admin=NULL;
    Komunikacija_sa_Serverom* komunikacija;
    QTimer t;
    bool da_li_je_tajmer_otkucao;
    int id_komanda_predmeti=-1;
    int id_komanda_profesori=-1;
    int flag_dodaj_predmet=-1;
    int id_komanda_logovani_prof=-1;
    int id_profesora_za_dodaj_predmet=-1;
    QString ime_predmeta_za_dodaj_predmet="";
    int flag_brisi_predmet=-1;
    int id_predmeta_za_brisanje=-1;
    int id_profesora_za_brisi_predmet=-1;

    int flag_brisi_profesor=-1;
    int id_profesora_koji_se_brise=-1;
    Predmet* snimljen_Predmet=NULL;
    Profesor* snimljen_Profesor=NULL;



signals:
    void stigo_odg_od_komunikacije(Odgovor_na_Komandu* odg);
    void refresh_predmete();
    void refresh_profesore();

    void signal_greska(Error* greska);
    void refresh_logovanje(Odgovor_na_Komandu* odg);
    void signal_stigli_predmeti();
    void signal_stigli_profesori();
    void signal_profesor_je_logovan(QString profa);
    void signal_error_diskonektovan_sa_servera();

public slots:

    void primi_odgovor_na_komandu(Odgovor_na_Komandu* kom);

    void otkucao_tajmer();

};

#endif // CONTROLLER_H
