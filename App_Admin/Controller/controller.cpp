#include "controller.h"

Controller::Controller(QObject *parent) : QObject(parent)
{
    t.setInterval(30000);
    t.setSingleShot(true);
    connect(&t,SIGNAL(timeout()),this,SLOT(otkucao_tajmer()));
    da_li_je_tajmer_otkucao=true;

}
void Controller::primi_odgovor_na_komandu(Odgovor_na_Komandu *kom)
{
    switch( kom->vrati_tip())
    {
    case ODGOVOR_LOGOVANJE:
    {
        odgovor_logovanje* odg=qobject_cast<odgovor_logovanje*>(kom);
        this->postavi_administratora(odg->vratiId(),
                                odg->vratiIme(),
                                odg->vratiPrezime(),
                                odg->vratiEmail(),
                                odg->vratiUsername(),
                                odg->vrati_Password());
        emit refresh_logovanje(kom);
        break;
    }
    case ODGOVOR_PREDMETI:
    {
        if(id_komanda_predmeti!=-1)
        {
            Odgovor_Predmeti *opredmeti=qobject_cast<Odgovor_Predmeti*>(kom);
            this->predmeti.clear();
            this->predmeti=opredmeti->vratiListu();
            id_komanda_predmeti=-1;
            emit signal_stigli_predmeti();
        }
        break;
    }
    case ODGOVOR_PROFESORI:
    {
        if(id_komanda_profesori!=-1)
        {
        Odgovor_Profesori* oprofesori=qobject_cast<Odgovor_Profesori*>(kom);
        this->profesori.clear();
        this->profesori=oprofesori->vratiListu();
        id_komanda_profesori=-1;
        emit signal_stigli_profesori();
        }
        if(id_komanda_logovani_prof!=-1)
        {
            Odgovor_Profesori* oprofesori=qobject_cast<Odgovor_Profesori*>(kom);
            this->logovani_profesori.clear();
            this->logovani_profesori=oprofesori->vratiListu();
            if(flag_dodaj_predmet!=-1)
            {
              this->dodaj_predmet_2();
              id_komanda_logovani_prof=-1;
            }
            if(flag_brisi_predmet!=-1)
             {
               this->brisi_predmet_2();
               id_komanda_logovani_prof=-1;
             }
            if(flag_brisi_profesor!=-1)
            {
                this->brisi_profesora_2();
                id_komanda_logovani_prof=-1;
            }
        }
        break;
    }
    case ODGOVOR_SNIMLJEN:
    {
        if(snimljen_Predmet!=NULL)//predmet
        {
            Odgovor_Snimljen* osnimljen= qobject_cast<Odgovor_Snimljen*>(kom);
            snimljen_Predmet->postavi_ID(osnimljen->vrati_ID());
            this->predmeti.append(*snimljen_Predmet);
            snimljen_Predmet=NULL;
            flag_dodaj_predmet=-1;
            id_profesora_za_dodaj_predmet=-1;
            ime_predmeta_za_dodaj_predmet="";
            emit refresh_predmete();
        }
        else if(snimljen_Profesor!=NULL)
        {
            Odgovor_Snimljen* osnimljen= qobject_cast<Odgovor_Snimljen*>(kom);
            snimljen_Profesor->promeniId(osnimljen->vrati_ID());
            this->profesori.append(*snimljen_Profesor);
            snimljen_Profesor=NULL;
            emit refresh_profesore();
        }
        break;
    }
    case ODGOVOR_OBRISAN:
    {
        if(id_predmeta_za_brisanje!=-1)
        {
            for(int i=0; i<predmeti.count();i++)
            {
                if(predmeti[i].vrati_ID()==id_predmeta_za_brisanje)
                {
                    predmeti.removeAt(i);
                    i--;
                    break;
                }
            }
            id_predmeta_za_brisanje=-1;
            flag_brisi_predmet=-1;
            id_profesora_za_brisi_predmet=-1;
            emit refresh_predmete();
            emit refresh_profesore();
        }
        if(id_profesora_koji_se_brise!=-1)
        {
            for(int i=0; i<profesori.count();i++)
            {
                if(profesori[i].vrati_ID()==id_profesora_koji_se_brise)
                {
                   profesori.removeAt(i);
                   i--;
                   break;
                }
            }
            for(int j=0; j<predmeti.count();j++)
            {
                if(predmeti[j].vrati_ID_Profesora()==id_profesora_koji_se_brise)
                {
                    predmeti.removeAt(j);
                    j--;
                }
            }
            flag_brisi_profesor=-1;
            id_profesora_koji_se_brise=-1;
            emit refresh_profesore();
            emit refresh_predmete();
        }
        QMessageBox::warning(NULL,"Kviz Bumerang","Obrisano!",QMessageBox::Ok,QMessageBox::NoButton);
        break;
    }
    case ERROR:
    {
        Error* greska=qobject_cast<Error*>(kom);
        emit signal_greska(greska);
        break;
    }
    case ERROR_DISKONEKTOVAN_SA_SERVERA:
    {
        //sve forme koje su trenutno aktivne da se zatvore, da se pobrisu predmeti(ceo model)
        //i da se otvori Forma Logovanje i da se izbaci greksa
        emit signal_error_diskonektovan_sa_servera();
        break;
    }
    default:
    {
        Error* greska=new Error();
        greska->postavi_greska("Dosao je odgovor ali ne umem da ga razumem");
        emit signal_greska(greska);
        break;
    }
 }
delete kom;
}
void Controller::logovanje_administratora(QString u, QString p)
{
        Komanda_Logovanje *klog=new Komanda_Logovanje();
        klog->postavi_pass(p);
        klog->postavi_user(u);
        klog->postavi_tabelu(ADMIN);
        komunikacija->salji_komandu(klog);
}
void Controller::otkucao_tajmer()
{

}
int Controller::vrati_id_profesora(QString email)
{
    int indeks=-1;
    for(int i=0; i<profesori.count();i++)
    {
        if(profesori[i].vrati_Email()==email)
        {
                indeks=profesori[i].vrati_ID();
                break;
        }
    }
    return indeks;
}
QList<Predmet> Controller::komanda_vrati_predmete_profesora(QString email_profesora)
{
   int id_profesora=vrati_id_profesora(email_profesora);
   QList<Predmet> p;
   for(int i=0; i<predmeti.count();i++)
   {
       if(predmeti[i].vrati_ID_Profesora()==id_profesora)
       {
           p.append(predmeti[i]);
       }
   }
   return p;
}

void Controller::dodaj_predmet(QString ime_predmeta, QString email_profesora)
{
    flag_dodaj_predmet=1;
    id_komanda_logovani_prof=1;
    id_profesora_za_dodaj_predmet=vrati_id_profesora(email_profesora);
    ime_predmeta_za_dodaj_predmet=ime_predmeta;
    komanda_vrati_sve_logovane_profesore* klogovani=new komanda_vrati_sve_logovane_profesore();
    komunikacija->salji_komandu(klogovani);
}
QString Controller::vrati_podatke(int id)
{
    for(int i=0; i<profesori.count();i++)
    {
        if(profesori[i].vrati_ID()==id)
        {
            return (profesori[i].vrati_Ime()+ " "+ profesori[i].vrati_Prezime()+ " " +profesori[i].vrati_Email());
        }
    }
}

void Controller::dodaj_predmet_2()
{
    bool jeste_logovan=false;
    for(int i=0; i<logovani_profesori.count();i++)
    {
        if(logovani_profesori[i].vrati_ID()==id_profesora_za_dodaj_predmet)//id_profesora
        {
            jeste_logovan=true;
            break;
        }
    }
    if(jeste_logovan==true)
    {
             QString pomocna=vrati_podatke(id_profesora_za_dodaj_predmet);
             flag_dodaj_predmet=-1;
             id_profesora_za_dodaj_predmet=-1;
             ime_predmeta_za_dodaj_predmet="";
             emit signal_profesor_je_logovan(pomocna);
    }
    else
   {
    snimljen_Predmet=new Predmet();
    snimljen_Predmet->postavi_ime(ime_predmeta_za_dodaj_predmet);
    snimljen_Predmet->postavi_ID_Profesora(id_profesora_za_dodaj_predmet);
    komandadodaj_predmet* kpredmet=new komandadodaj_predmet();
    kpredmet->promeniIdProfesora(id_profesora_za_dodaj_predmet);
    kpredmet->promeniImePredmeta(ime_predmeta_za_dodaj_predmet);
    komunikacija->salji_komandu(kpredmet);
    }
}
void Controller::brisi_predmet(QString ime_predmeta)
{
    flag_brisi_predmet=1;
    id_komanda_logovani_prof=1;
    for(int i=0; i<predmeti.count();i++)
    {
        if(predmeti[i].vrati_ime()==ime_predmeta)
        {
            id_profesora_za_brisi_predmet=predmeti[i].vrati_ID_Profesora();
            break;
        }
    }
    id_predmeta_za_brisanje=this->vrati_id_predmeta(ime_predmeta);
    komanda_vrati_sve_logovane_profesore* klogovani=new komanda_vrati_sve_logovane_profesore();
    komunikacija->salji_komandu(klogovani);
}
void Controller::brisi_predmet_2()
{
    bool jeste_logovan=false;
    for(int i=0; i<logovani_profesori.count();i++)
    {
        if(logovani_profesori[i].vrati_ID()==id_profesora_za_brisi_predmet)//id_profesora
        {
            jeste_logovan=true;
            break;
        }
    }
    if(jeste_logovan==true)
    {        QString pomocna=vrati_podatke(id_profesora_za_brisi_predmet);
             flag_brisi_predmet=-1;
             id_profesora_za_brisi_predmet=-1;
             id_predmeta_za_brisanje=-1;
             emit signal_profesor_je_logovan(pomocna);
    }
    else
    {
        KomandaBrisi *kbrisi=new KomandaBrisi();
        kbrisi->PromeniTabelu(TABELA_PREDMET);
        kbrisi->PromeniUslov(TABELA_PREDMET_ID,id_predmeta_za_brisanje);
        komunikacija->salji_komandu(kbrisi);
    }
}
int Controller::vrati_id_predmeta(QString predmet)
{
    int id=-1;
    for(int i=0; i<predmeti.count();i++)
    {
        if(predmeti[i].vrati_ime()==predmet)
        {
            id=predmeti[i].vrati_ID();
            break;
        }
    }
    return id;
}
Profesor Controller::komanda_vrati_profesora_predmeta(QString ime_predmeta)
{
    int id_predmeta=vrati_id_predmeta(ime_predmeta);
    int id_profesora=-1;
    for(int j=0; j<predmeti.count();j++)
    {
        if(predmeti[j].vrati_ID()==id_predmeta)
        {
            id_profesora=predmeti[j].vrati_ID_Profesora();
            break;
        }
    }
    for(int k=0; k<profesori.count();k++)
    {
        if(profesori[k].vrati_ID()==id_profesora)
        {
            return profesori[k];
        }
    }
}
void Controller::dodaj_profesora(QString ime, QString prezime, QString email, QString us, QString pass)
{
    komandadodaj_profesor* kprofesor=new komandadodaj_profesor();
    snimljen_Profesor=new Profesor();
    snimljen_Profesor->promeniEmail(email);
    snimljen_Profesor->promeniIme(ime);
    snimljen_Profesor->promeniPrezime(prezime);
    snimljen_Profesor->promeniPassword(pass);
    snimljen_Profesor->promeniUsername(us);

    kprofesor->promeniIme(ime);
    kprofesor->promeniPrezime(prezime);
    kprofesor->promeniEmail(email);
    kprofesor->promeniUsername(us);
    kprofesor->promeniPassword(pass);
    komunikacija->salji_komandu(kprofesor);
}
void Controller::brisi_profesora(QString email_profesora)
{
    flag_brisi_profesor=1;
    id_komanda_logovani_prof=1;
    id_profesora_koji_se_brise=vrati_id_profesora(email_profesora);
    komanda_vrati_sve_logovane_profesore* klogovani=new komanda_vrati_sve_logovane_profesore();
    komunikacija->salji_komandu(klogovani);
}
void Controller::brisi_profesora_2()
{
    bool jeste_logovan=false;
    for(int i=0; i<logovani_profesori.count();i++)
    {
        if(logovani_profesori[i].vrati_ID()==id_profesora_koji_se_brise)
        {
            jeste_logovan=true;
            break;
        }
    }
    if(jeste_logovan==true)
    {
             QString pomocna=vrati_podatke(id_profesora_koji_se_brise);
             flag_brisi_profesor=-1;
             id_profesora_koji_se_brise=-1;
             emit signal_profesor_je_logovan(pomocna);
    }
    else
    {
        KomandaBrisi *kbrisi=new KomandaBrisi();
        kbrisi->PromeniTabelu(TABELA_PROFESOR);
        kbrisi->PromeniUslov(TABELA_PROFESOR_ID,id_profesora_koji_se_brise);
        komunikacija->salji_komandu(kbrisi);
    }

}
















