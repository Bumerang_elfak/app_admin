/********************************************************************************
** Form generated from reading UI file 'dodajpredmet.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DODAJPREDMET_H
#define UI_DODAJPREDMET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_DodajPredmet
{
public:
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLineEdit *nazivPredmetaEdit;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_3;
    QTableView *profesorZaPredmetView;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *DodajPredmet)
    {
        if (DodajPredmet->objectName().isEmpty())
            DodajPredmet->setObjectName(QStringLiteral("DodajPredmet"));
        DodajPredmet->resize(477, 302);
        verticalLayout_2 = new QVBoxLayout(DodajPredmet);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label = new QLabel(DodajPredmet);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout->addWidget(label);

        nazivPredmetaEdit = new QLineEdit(DodajPredmet);
        nazivPredmetaEdit->setObjectName(QStringLiteral("nazivPredmetaEdit"));
        nazivPredmetaEdit->setMaxLength(40);

        verticalLayout->addWidget(nazivPredmetaEdit);


        verticalLayout_3->addLayout(verticalLayout);


        verticalLayout_2->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        label_3 = new QLabel(DodajPredmet);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout_4->addWidget(label_3);

        profesorZaPredmetView = new QTableView(DodajPredmet);
        profesorZaPredmetView->setObjectName(QStringLiteral("profesorZaPredmetView"));
        profesorZaPredmetView->setSelectionBehavior(QAbstractItemView::SelectRows);

        verticalLayout_4->addWidget(profesorZaPredmetView);


        verticalLayout_2->addLayout(verticalLayout_4);

        buttonBox = new QDialogButtonBox(DodajPredmet);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout_2->addWidget(buttonBox);


        retranslateUi(DodajPredmet);
        QObject::connect(buttonBox, SIGNAL(accepted()), DodajPredmet, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), DodajPredmet, SLOT(reject()));

        QMetaObject::connectSlotsByName(DodajPredmet);
    } // setupUi

    void retranslateUi(QDialog *DodajPredmet)
    {
        DodajPredmet->setWindowTitle(QApplication::translate("DodajPredmet", "Dialog", 0));
        label->setText(QApplication::translate("DodajPredmet", "Unesite naziv predmeta:", 0));
        label_3->setText(QApplication::translate("DodajPredmet", "Selektujte profesora:", 0));
    } // retranslateUi

};

namespace Ui {
    class DodajPredmet: public Ui_DodajPredmet {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DODAJPREDMET_H
