#ifndef ADMINWINDOW_H
#define ADMINWINDOW_H
#include "dodajpredmet.h"
#include "dodajprofesora.h"
#include "View/view.h"
#include <QMainWindow>
#include <QCloseEvent>

namespace Ui {
class AdminWindow;
}

class AdminWindow : public View
{
    Q_OBJECT

public:
    explicit AdminWindow(QWidget *parent = 0);
    virtual void AddListener(Controller* controller)
    {
        this->controller=controller;
        connect(controller, SIGNAL(signal_greska(Error*)),this,SLOT(odgovor_error(Error*)));
        connect(controller,SIGNAL(signal_profesor_je_logovan(QString)),this,SLOT(profesor_je_logovan(QString)));
        connect(controller,SIGNAL(refresh_predmete()),this,SLOT(osvezi_predmete()));
        connect(controller,SIGNAL(refresh_profesore()),this,SLOT(osvezi_profesore()));
        connect(this->controller, SIGNAL(signal_error_diskonektovan_sa_servera()), this, SLOT(odgovor_diskonektovan())) ;

    }
    ~AdminWindow();

protected:
    void closeEvent(QCloseEvent *event);

private slots:
    void on_dodajPredmetButton_clicked();

    void on_brisiPredmetButton_clicked();

    void on_dodajProfesoraButton_clicked();

    void on_brisiProfesoraButton_clicked();

    void on_profesoriView_clicked(const QModelIndex &index);

    void on_predmetiView_clicked(const QModelIndex &index);

private:

    Ui::AdminWindow *ui;
    DodajPredmet * dpredmeta;
    DodajProfesora* dprofesora;


public slots:

    void prikaz_Predmeta();
    void prikaz_Profesora();
    void odgovor_error(Error* greska);
    void odgovor_diskonektovan();
    void profesor_je_logovan(QString profa);
    void osvezi_predmete();
    void osvezi_profesore();

};

#endif // ADMINWINDOW_H
