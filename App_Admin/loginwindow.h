#ifndef LOGINWINDOW_H
#define LOGINWINDOW_H
#include "adminwindow.h"
#include "Controller/controller.h"
#include "View/view.h"

#include <QMainWindow>

namespace Ui {
class LoginWindow;
}

class LoginWindow : public View
{
    Q_OBJECT

public:
    explicit LoginWindow(QWidget *parent = 0);
    virtual void AddListener(Controller* controller)
    {
        this->controller=controller;
        connect(controller, SIGNAL(refresh_logovanje(Odgovor_na_Komandu*)),this,SLOT(odgovor_na_logovanje(Odgovor_na_Komandu*)));
        connect(controller, SIGNAL(signal_greska(Error*)),this,SLOT(odgovor_error(Error*)));
        connect(controller,SIGNAL(signal_stigli_predmeti()),this,SLOT(odgovor_na_stigli_predmeti()));
        connect(controller,SIGNAL(signal_stigli_profesori()),this,SLOT(odgovor_na_stigli_profesori()));
        connect(this->controller, SIGNAL(signal_error_diskonektovan_sa_servera()), this, SLOT(odgovor_diskonektovan())) ;

    }

    ~LoginWindow();

private slots:
    void on_loginButton_clicked();



    void on_usernameEdit_returnPressed();

    void on_passwordEdit_returnPressed();

private:
    Ui::LoginWindow *ui;
    AdminWindow* aw;

public slots:
     void odgovor_na_logovanje(Odgovor_na_Komandu *odg);
     void odgovor_error(Error* greska);
     void odgovor_na_stigli_predmeti();
     void odgovor_na_stigli_profesori();
     void odgovor_diskonektovan()
     {
         this->close();
         //this->deleteLater();
     }


};

#endif // LOGINWINDOW_H
