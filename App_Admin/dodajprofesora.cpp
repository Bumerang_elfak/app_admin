#include "dodajprofesora.h"
#include "ui_dodajprofesora.h"
#include <QFileDialog>
#include <QTimer>
#include <QMessageBox>
#include <QDebug>
#include <QWidget>

DodajProfesora::DodajProfesora(QWidget *parent) :
    view2(parent),
    ui(new Ui::DodajProfesora)
{
    ui->setupUi(this);
    QIcon ikonica(":/Slike/Ikonica.ico");
    this->setWindowIcon(ikonica);
    this->setWindowTitle("Kviz Bumerang");
}

DodajProfesora::~DodajProfesora()
{
    delete ui;
}

void DodajProfesora::on_buttonBox_accepted()
{
    if(ui->imeEdit->text().isEmpty())
    {
        QMessageBox::warning(this,"Kviz Bumernag","Niste uneli ime profesora!",QMessageBox::Ok,QMessageBox::NoButton);
        this->setVisible(true);
        return;
    }
    else if(ui->prezimeEdit->text().isEmpty())
    {
        QMessageBox::warning(this,"Kviz Bumernag","Niste uneli prezime profesora!",QMessageBox::Ok,QMessageBox::NoButton);
        this->setVisible(true);
        return;
    }
    else if(ui->emailEdit->text().isEmpty())
    {
        QMessageBox::warning(this,"Kviz Bumernag","Niste uneli email profesora!",QMessageBox::Ok,QMessageBox::NoButton);
        this->setVisible(true);
        return;
    }
    else if(ui->usernameEdit->text().isEmpty())
    {
        QMessageBox::warning(this,"Kviz Bumernag","Niste uneli username profesora!",QMessageBox::Ok,QMessageBox::NoButton);
        this->setVisible(true);
        return;
    }
    else if(ui->passwordEdit->text().isEmpty())
    {
        QMessageBox::warning(this,"Kviz Bumernag","Niste uneli password profesora!",QMessageBox::Ok,QMessageBox::NoButton);
        this->setVisible(true);
        return;
    }
    else
    {
        controller->dodaj_profesora(ui->imeEdit->text(),ui->prezimeEdit->text(),
                                    ui->emailEdit->text(),ui->usernameEdit->text(),ui->passwordEdit->text());
        on_DodajProfesora_finished(QDialog::Accepted);
        this->deleteLater();
    }

}


void DodajProfesora::on_DodajProfesora_finished(int result)
{
    this->setVisible(true);
    if( result==QDialog::Rejected ){

        int odg=QMessageBox::question(this,"Save?","Jeste li sigurni da zelite da izadjete?",QMessageBox::Ok,QMessageBox::Cancel);
        if( odg==QMessageBox::Ok ){
            on_DodajProfesora_finished(QDialog::Accepted);
            this->deleteLater();
        }
        else
            this->setVisible(true);
    }
}
