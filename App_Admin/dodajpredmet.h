#ifndef DODAJPREDMET_H
#define DODAJPREDMET_H
#include "View/view2.h"
#include <QDialog>

namespace Ui {
class DodajPredmet;
}

class DodajPredmet : public view2
{
    Q_OBJECT

public:
    explicit DodajPredmet(QWidget *parent = 0);
    virtual void AddListener(Controller* controller)
    {
        this->controller=controller;
        connect(this->controller, SIGNAL(signal_error_diskonektovan_sa_servera()), this, SLOT(odgovor_diskonektovan())) ;


    }
    ~DodajPredmet();

private slots:
    void on_buttonBox_accepted();

    //void on_buttonBox_rejected();

    void on_DodajPredmet_finished(int result);

private:
    Ui::DodajPredmet *ui;

public slots:
    void prikaz_Profesora();
    void odgovor_diskonektovan()
    {
        this->close();
        this->deleteLater();
    }


};

#endif // DODAJPREDMET_H
