/********************************************************************************
** Form generated from reading UI file 'loginwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINWINDOW_H
#define UI_LOGINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LoginWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QLabel *label_Admin;
    QVBoxLayout *verticalLayout_5;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_2;
    QLineEdit *usernameEdit;
    QVBoxLayout *verticalLayout_7;
    QLabel *label_3;
    QLineEdit *passwordEdit;
    QPushButton *loginButton;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *LoginWindow)
    {
        if (LoginWindow->objectName().isEmpty())
            LoginWindow->setObjectName(QStringLiteral("LoginWindow"));
        LoginWindow->resize(487, 258);
        centralwidget = new QWidget(LoginWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label_Admin = new QLabel(centralwidget);
        label_Admin->setObjectName(QStringLiteral("label_Admin"));
        QFont font;
        font.setPointSize(22);
        label_Admin->setFont(font);
        label_Admin->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_Admin);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setStyleSheet(QString::fromUtf8("background-image: url(:/slike/\320\277\321\200\320\265\321\203\320\267\320\270\320\274\320\260\321\232\320\265 (2).png);\n"
"font: 11pt \"MS Shell Dlg 2\";\n"
"text-align: center;"));

        verticalLayout_6->addWidget(label_2);

        usernameEdit = new QLineEdit(centralwidget);
        usernameEdit->setObjectName(QStringLiteral("usernameEdit"));
        usernameEdit->setEnabled(true);
        usernameEdit->setCursor(QCursor(Qt::ArrowCursor));
        usernameEdit->setStyleSheet(QStringLiteral("background-image: url(:/slike/white3.jpg);"));
        usernameEdit->setMaxLength(30);
        usernameEdit->setFrame(true);
        usernameEdit->setCursorPosition(0);
        usernameEdit->setCursorMoveStyle(Qt::LogicalMoveStyle);
        usernameEdit->setClearButtonEnabled(true);

        verticalLayout_6->addWidget(usernameEdit);


        verticalLayout_5->addLayout(verticalLayout_6);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setStyleSheet(QString::fromUtf8("background-image: url(:/slike/\320\277\321\200\320\265\321\203\320\267\320\270\320\274\320\260\321\232\320\265 (2).png);\n"
"font: 11pt \"MS Shell Dlg 2\";\n"
"text-align: center;"));

        verticalLayout_7->addWidget(label_3);

        passwordEdit = new QLineEdit(centralwidget);
        passwordEdit->setObjectName(QStringLiteral("passwordEdit"));
        passwordEdit->setStyleSheet(QStringLiteral("background-image: url(:/slike/white3.jpg);"));
        passwordEdit->setMaxLength(30);
        passwordEdit->setEchoMode(QLineEdit::Normal);
        passwordEdit->setDragEnabled(false);
        passwordEdit->setReadOnly(false);
        passwordEdit->setCursorMoveStyle(Qt::LogicalMoveStyle);
        passwordEdit->setClearButtonEnabled(true);

        verticalLayout_7->addWidget(passwordEdit);


        verticalLayout_5->addLayout(verticalLayout_7);


        verticalLayout->addLayout(verticalLayout_5);

        loginButton = new QPushButton(centralwidget);
        loginButton->setObjectName(QStringLiteral("loginButton"));
        loginButton->setStyleSheet(QStringLiteral(""));

        verticalLayout->addWidget(loginButton);

        LoginWindow->setCentralWidget(centralwidget);
        statusbar = new QStatusBar(LoginWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        LoginWindow->setStatusBar(statusbar);

        retranslateUi(LoginWindow);

        QMetaObject::connectSlotsByName(LoginWindow);
    } // setupUi

    void retranslateUi(QMainWindow *LoginWindow)
    {
        LoginWindow->setWindowTitle(QApplication::translate("LoginWindow", "MainWindow", 0));
        label_Admin->setText(QApplication::translate("LoginWindow", "ADMIN", 0));
        label_2->setText(QApplication::translate("LoginWindow", "Username:", 0));
        usernameEdit->setText(QString());
        label_3->setText(QApplication::translate("LoginWindow", "Password:", 0));
        passwordEdit->setInputMask(QString());
        loginButton->setText(QApplication::translate("LoginWindow", "Login ", 0));
    } // retranslateUi

};

namespace Ui {
    class LoginWindow: public Ui_LoginWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINWINDOW_H
