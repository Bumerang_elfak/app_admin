/********************************************************************************
** Form generated from reading UI file 'adminwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADMINWINDOW_H
#define UI_ADMINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AdminWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;
    QWidget *tab;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_5;
    QLabel *label_5;
    QTableView *predmetiView;
    QLabel *label_4;
    QTableView *profesorPredmetaView;
    QVBoxLayout *verticalLayout_3;
    QSpacerItem *verticalSpacer_2;
    QPushButton *dodajPredmetButton;
    QPushButton *brisiPredmetButton;
    QWidget *tab_2;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_3;
    QTableView *profesoriView;
    QLabel *label_2;
    QTableView *predmetiProfesoraView;
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout_2;
    QSpacerItem *verticalSpacer;
    QPushButton *dodajProfesoraButton;
    QPushButton *brisiProfesoraButton;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *AdminWindow)
    {
        if (AdminWindow->objectName().isEmpty())
            AdminWindow->setObjectName(QStringLiteral("AdminWindow"));
        AdminWindow->resize(822, 402);
        centralwidget = new QWidget(AdminWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setEnabled(true);
        QFont font;
        font.setFamily(QStringLiteral("MS PMincho"));
        font.setPointSize(22);
        font.setBold(true);
        font.setWeight(75);
        tabWidget->setFont(font);
        tabWidget->setCursor(QCursor(Qt::ArrowCursor));
        tabWidget->setMouseTracking(true);
        tabWidget->setAutoFillBackground(false);
        tabWidget->setTabPosition(QTabWidget::North);
        tabWidget->setTabShape(QTabWidget::Rounded);
        tabWidget->setIconSize(QSize(79, 95));
        tabWidget->setElideMode(Qt::ElideLeft);
        tabWidget->setMovable(true);
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        tab->setMouseTracking(false);
        tab->setStyleSheet(QStringLiteral(""));
        horizontalLayout = new QHBoxLayout(tab);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        label_5 = new QLabel(tab);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setStyleSheet(QString::fromUtf8("background-image: url(:/slike/\320\277\321\200\320\265\321\203\320\267\320\270\320\274\320\260\321\232\320\265 (2).png);\n"
"font: 11pt \"MS Shell Dlg 2\";"));

        verticalLayout_5->addWidget(label_5);

        predmetiView = new QTableView(tab);
        predmetiView->setObjectName(QStringLiteral("predmetiView"));

        verticalLayout_5->addWidget(predmetiView);

        label_4 = new QLabel(tab);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setStyleSheet(QString::fromUtf8("background-image: url(:/slike/\320\277\321\200\320\265\321\203\320\267\320\270\320\274\320\260\321\232\320\265 (2).png);\n"
"font: 11pt \"MS Shell Dlg 2\";"));

        verticalLayout_5->addWidget(label_4);

        profesorPredmetaView = new QTableView(tab);
        profesorPredmetaView->setObjectName(QStringLiteral("profesorPredmetaView"));

        verticalLayout_5->addWidget(profesorPredmetaView);


        horizontalLayout->addLayout(verticalLayout_5);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_2);

        dodajPredmetButton = new QPushButton(tab);
        dodajPredmetButton->setObjectName(QStringLiteral("dodajPredmetButton"));
        dodajPredmetButton->setStyleSheet(QString::fromUtf8("background-image: url(:/slike/\320\277\321\200\320\265\321\203\320\267\320\270\320\274\320\260\321\232\320\265 (2).png);\n"
"font:  10pt \"MS Shell Dlg 2\";\n"
""));

        verticalLayout_3->addWidget(dodajPredmetButton);

        brisiPredmetButton = new QPushButton(tab);
        brisiPredmetButton->setObjectName(QStringLiteral("brisiPredmetButton"));
        brisiPredmetButton->setStyleSheet(QString::fromUtf8("background-image: url(:/slike/\320\277\321\200\320\265\321\203\320\267\320\270\320\274\320\260\321\232\320\265 (2).png);\n"
"font:  10pt \"MS Shell Dlg 2\";\n"
""));

        verticalLayout_3->addWidget(brisiPredmetButton);


        horizontalLayout->addLayout(verticalLayout_3);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        horizontalLayout_3 = new QHBoxLayout(tab_2);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        label_3 = new QLabel(tab_2);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setStyleSheet(QString::fromUtf8("background-image: url(:/slike/\320\277\321\200\320\265\321\203\320\267\320\270\320\274\320\260\321\232\320\265 (2).png);\n"
"font: 11pt \"MS Shell Dlg 2\";"));

        verticalLayout_4->addWidget(label_3);

        profesoriView = new QTableView(tab_2);
        profesoriView->setObjectName(QStringLiteral("profesoriView"));

        verticalLayout_4->addWidget(profesoriView);

        label_2 = new QLabel(tab_2);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setStyleSheet(QString::fromUtf8("background-image: url(:/slike/\320\277\321\200\320\265\321\203\320\267\320\270\320\274\320\260\321\232\320\265 (2).png);\n"
"font: 11pt \"MS Shell Dlg 2\";"));

        verticalLayout_4->addWidget(label_2);

        predmetiProfesoraView = new QTableView(tab_2);
        predmetiProfesoraView->setObjectName(QStringLiteral("predmetiProfesoraView"));

        verticalLayout_4->addWidget(predmetiProfesoraView);


        horizontalLayout_3->addLayout(verticalLayout_4);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        dodajProfesoraButton = new QPushButton(tab_2);
        dodajProfesoraButton->setObjectName(QStringLiteral("dodajProfesoraButton"));
        dodajProfesoraButton->setStyleSheet(QString::fromUtf8("background-image: url(:/slike/\320\277\321\200\320\265\321\203\320\267\320\270\320\274\320\260\321\232\320\265 (2).png);\n"
"font:  10pt \"MS Shell Dlg 2\";\n"
""));

        verticalLayout_2->addWidget(dodajProfesoraButton);

        brisiProfesoraButton = new QPushButton(tab_2);
        brisiProfesoraButton->setObjectName(QStringLiteral("brisiProfesoraButton"));
        brisiProfesoraButton->setStyleSheet(QString::fromUtf8("background-image: url(:/slike/\320\277\321\200\320\265\321\203\320\267\320\270\320\274\320\260\321\232\320\265 (2).png);\n"
"font:  10pt \"MS Shell Dlg 2\";\n"
""));

        verticalLayout_2->addWidget(brisiProfesoraButton);


        gridLayout->addLayout(verticalLayout_2, 0, 0, 1, 1);


        horizontalLayout_3->addLayout(gridLayout);

        tabWidget->addTab(tab_2, QString());

        verticalLayout->addWidget(tabWidget);

        AdminWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(AdminWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 822, 21));
        AdminWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(AdminWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        AdminWindow->setStatusBar(statusbar);

        retranslateUi(AdminWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(AdminWindow);
    } // setupUi

    void retranslateUi(QMainWindow *AdminWindow)
    {
        AdminWindow->setWindowTitle(QApplication::translate("AdminWindow", "MainWindow", 0));
        label_5->setText(QApplication::translate("AdminWindow", "Spisak svih predmeta", 0));
        label_4->setText(QApplication::translate("AdminWindow", "Za selektovani predmet, pregled profesora:", 0));
        dodajPredmetButton->setText(QApplication::translate("AdminWindow", "Dodaj", 0));
        brisiPredmetButton->setText(QApplication::translate("AdminWindow", "Brisi", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("AdminWindow", "Predmeti                         ", 0));
        label_3->setText(QApplication::translate("AdminWindow", "Spisak svih profesora", 0));
        label_2->setText(QApplication::translate("AdminWindow", "Za selektovanog profesora, pregled predmeta koji dr\305\276i:", 0));
        dodajProfesoraButton->setText(QApplication::translate("AdminWindow", "Dodaj", 0));
        brisiProfesoraButton->setText(QApplication::translate("AdminWindow", "Brisi", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("AdminWindow", "Profesori                       ", 0));
    } // retranslateUi

};

namespace Ui {
    class AdminWindow: public Ui_AdminWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADMINWINDOW_H
