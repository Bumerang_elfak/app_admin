#include <QApplication>
#include "loginwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Controller* con=new Controller();
    con->postavi_komunikaciju();

    QFile file(":/izgled.css");
    if(file.open(QIODevice::ReadOnly | QIODevice::Text)){
        a.setStyleSheet(file.readAll());
        file.close();
    }


    LoginWindow *w=new LoginWindow();
    w->AddListener(con);
    w->show();
    return a.exec();
}
