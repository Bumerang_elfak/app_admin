#include "dodajpredmet.h"
#include "ui_dodajpredmet.h"
#include <QFileDialog>
#include <QTimer>
#include <QMessageBox>
#include <QDebug>
#include <QWidget>
#include <QStandardItem>
#include <QAbstractItemModel>
#include <QAbstractItemView>

DodajPredmet::DodajPredmet(QWidget *parent) :
   view2(parent),
    ui(new Ui::DodajPredmet)
{
    ui->setupUi(this);

    ui->profesorZaPredmetView->setSelectionBehavior(QAbstractItemView::SelectRows);//cela vrsta selektovana
    ui->profesorZaPredmetView->setSelectionMode(QAbstractItemView::SingleSelection);//moze samo 1

    QIcon ikonica(":/Slike/Ikonica.ico");
    this->setWindowIcon(ikonica);
    this->setWindowTitle("Kviz Bumerang");
}
DodajPredmet::~DodajPredmet()
{
    delete ui;
}

void DodajPredmet::prikaz_Profesora()
{
    QList<Profesor> profesori=this->controller->vrati_profesore();
    QStandardItemModel * model = new QStandardItemModel(profesori.count(),5,this); //x Rows and 5 Columns
    model->setHorizontalHeaderItem(0, new QStandardItem(QString("Ime")));
    model->setHorizontalHeaderItem(1, new QStandardItem(QString("Prezime")));
    model->setHorizontalHeaderItem(2, new QStandardItem(QString("Email")));
    model->setHorizontalHeaderItem(3, new QStandardItem(QString("Username")));
    model->setHorizontalHeaderItem(4, new QStandardItem(QString("Password")));

    for(int i = 0; i < profesori.count() ; i++)//za svaku vrstu
    {
                model->setItem(i,0,new QStandardItem(QString(profesori[i].vrati_Ime())));
                model->setItem(i,1,new QStandardItem(QString(profesori[i].vrati_Prezime())));
                model->setItem(i,2,new QStandardItem(QString(profesori[i].vrati_Email())));
                model->setItem(i,3,new QStandardItem(QString(profesori[i].vrati_Username())));
                model->setItem(i,4,new QStandardItem(QString(profesori[i].vrati_Password())));
    }
    ui->profesorZaPredmetView->setModel(model);
}

void DodajPredmet::on_buttonBox_accepted()
{
    int brSelektovan=ui->profesorZaPredmetView->selectionModel()->selectedRows().count();
    if( ui->nazivPredmetaEdit->text().isEmpty() && brSelektovan==0)
    {
        QMessageBox::warning(this,"Kviz Bumernag","Niste uneli sve podatke o predmetu i niste selektovali profesora!",QMessageBox::Ok,QMessageBox::NoButton);
        this->setVisible(true);
        return;
    }
    else if(brSelektovan==0)
    {
        QMessageBox::warning(this,"Kviz Bumernag","Niste selektovali profesora!",QMessageBox::Ok,QMessageBox::NoButton);
        this->setVisible(true);
        return;

    }
    else if(ui->nazivPredmetaEdit->text().isEmpty() )
    {
        QMessageBox::warning(this,"Kviz Bumernag","Niste uneli sve podatke o predmetu!",QMessageBox::Ok,QMessageBox::NoButton);
        this->setVisible(true);
        return;
    }
    else{
        QString naziv_predmeta=ui->nazivPredmetaEdit->text();
        int vrsta=ui->profesorZaPredmetView->selectionModel()->currentIndex().row();//broj vrste
        QString email_profesora=ui->profesorZaPredmetView->model()->data(ui->profesorZaPredmetView->model()->index(vrsta,2)).toString();
        //u 2.koloni email profesora
        this->controller->dodaj_predmet(naziv_predmeta, email_profesora);
        this->on_DodajPredmet_finished(QDialog::Accepted);
        this->deleteLater();
    }
}


/*
void DodajPredmet::on_buttonBox_rejected(){
    this->setVisible(true);
    int odg=QMessageBox::question(this,"Save?","Jeste li sigurni da zelite da izadjete?",QMessageBox::Ok,QMessageBox::Cancel);
    if( odg==QMessageBox::Ok ){
        on_DodajPredmet_finished(QDialog::Accepted);
        this->deleteLater();
    }
    else
        return;
}
*/

void DodajPredmet::on_DodajPredmet_finished(int result)
{
    this->setVisible(true);
    if( result==QDialog::Rejected ){

        int odg=QMessageBox::question(this,"Save?","Jeste li sigurni da zelite da izadjete?",QMessageBox::Ok,QMessageBox::Cancel);
        if( odg==QMessageBox::Ok ){
            on_DodajPredmet_finished(QDialog::Accepted);
            this->deleteLater();
        }
        else
            this->setVisible(true);
    }
}

