#ifndef DODAJPROFESORA_H
#define DODAJPROFESORA_H
#include "View/view2.h"

#include <QDialog>

namespace Ui {
class DodajProfesora;
}

class DodajProfesora : public view2
{
    Q_OBJECT

public:
    explicit DodajProfesora(QWidget *parent = 0);
    virtual void AddListener(Controller* controller)
    {
        this->controller=controller;
        connect(this->controller, SIGNAL(signal_error_diskonektovan_sa_servera()), this, SLOT(odgovor_diskonektovan())) ;


    }
    ~DodajProfesora();

private slots:
    void on_buttonBox_accepted();

    void on_DodajProfesora_finished(int result);

private:
    Ui::DodajProfesora *ui;

public slots:
    void odgovor_diskonektovan()
    {
        this->close();
        this->deleteLater();
    }




};

#endif // DODAJPROFESORA_H
