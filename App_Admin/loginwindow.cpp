#include "loginwindow.h"
#include "ui_loginwindow.h"
#include <qmessagebox.h>
#include "adminwindow.h"

LoginWindow::LoginWindow(QWidget *parent) :
    View(parent),
    ui(new Ui::LoginWindow)
{
    ui->setupUi(this);
    this->setWindowTitle( tr("Kviz Bumerang"));
    //QPixmap bkgnd("C:/Users/LolaS/Desktop/slike/retard2.jpg.jpg");
    //bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    //QPalette palette;
    //palette.setBrush(QPalette::Background, bkgnd);
    //this->setPalette(palette);
    //this->setFixedSize(500,500);
    ui->passwordEdit->setEchoMode(QLineEdit::Password);

    QIcon ikonica(":/Slike/Ikonica.ico");
    this->setWindowIcon(ikonica);
    this->setWindowTitle("Kviz Bumerang");
}

LoginWindow::~LoginWindow()
{
    delete ui;
}

void LoginWindow::on_loginButton_clicked()
{
    if(ui->usernameEdit->text().isEmpty() && ui->passwordEdit->text().isEmpty())
    {  QMessageBox::warning(this,"Kviz Bumernag","Niste uneli podatke! Unesite username i password.",QMessageBox::Ok,QMessageBox::NoButton);
        return;
    }
    if(ui->usernameEdit->text().isEmpty())
    {
        QMessageBox::warning(this,"Kviz Bumernag","Unesite username!",QMessageBox::Ok,QMessageBox::NoButton);
        ui->usernameEdit->setText("");
        ui->passwordEdit->setText("");
        return;
    }
    if(ui->passwordEdit->text().isEmpty())
    {
        QMessageBox::warning(this,"Kviz Bumernag","Unesite password!",QMessageBox::Ok,QMessageBox::NoButton);
        ui->usernameEdit->setText("");
        ui->passwordEdit->setText("");
        return;
    }
    QString username=ui->usernameEdit->text();
    QString password=ui->passwordEdit->text();

    this->controller->logovanje_administratora(username, password);
}

void LoginWindow::odgovor_na_logovanje(Odgovor_na_Komandu *odg)
{
    if(odg->vrati_tip()==ERROR)
    {
        Error* error=qobject_cast<Error*>(odg);
        QString greska=error->vrati_greska();
        QMessageBox::warning(this,"Kviz Bumerang",greska,QMessageBox::Ok,QMessageBox::NoButton);
        ui->passwordEdit->setText("");
        ui->usernameEdit->setText("");
        return;
    }
    else if(odg->vrati_tip()==ODGOVOR_LOGOVANJE)
    {
        odgovor_logovanje *odgovor=qobject_cast<odgovor_logovanje*>(odg);
        controller->komanda_vrati_predmete();
        //QMessageBox::warning(this,"Kviz Bumerang","Molim vas sacekajte odogovor-cekaju se predmeti",QMessageBox::Ok,QMessageBox::NoButton);
     }
}
void LoginWindow::odgovor_na_stigli_predmeti()
{
        controller->komanda_vrati_profesore();
        //QMessageBox::warning(this,"Kviz Bumerang","Molim vas sacekajte odogovor-cekaju se profesori",QMessageBox::Ok,QMessageBox::NoButton);

}
void LoginWindow::odgovor_na_stigli_profesori()
{
    this->close();
    aw=new AdminWindow();
    aw->AddListener(this->controller);
    aw->prikaz_Predmeta();
    aw->prikaz_Profesora();
    aw->show();
    this->deleteLater();
}
void LoginWindow::odgovor_error(Error *greska)
{
    QString naziv_greska=greska->vrati_greska();
    qDebug()<<naziv_greska;
    QMessageBox::warning(this,"Kviz Bumerang",naziv_greska,QMessageBox::Ok,QMessageBox::NoButton);
    ui->passwordEdit->setText("");
    ui->usernameEdit->setText("");
    return;
}

void LoginWindow::on_usernameEdit_returnPressed()
{
    on_loginButton_clicked();
}

void LoginWindow::on_passwordEdit_returnPressed()
{
    on_loginButton_clicked();
}
