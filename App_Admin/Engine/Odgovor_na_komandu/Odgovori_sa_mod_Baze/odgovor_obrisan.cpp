#include "Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_obrisan.h"

Odgovor_Obrisan::Odgovor_Obrisan()
{
    Tip=ODGOVOR_OBRISAN;
}
Odgovor_Obrisan::~Odgovor_Obrisan()
{

}

void Odgovor_Obrisan::postaviOK(bool novo)
{
    OK=novo;
}

bool Odgovor_Obrisan::vratiOK()
{
    return OK;
}

QByteArray  Odgovor_Obrisan::za_slanje()
{
    // TIP || OK
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(QString::number(OK?1:0)));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}

int Odgovor_Obrisan::praviOd(QString& izvorniQString)
{

    // TIP || OK
        using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=ODGOVOR_OBRISAN)
        return 0;

    //dekapsulacija
    Tip=ODGOVOR_OBRISAN;
    pomocna=dekapsulacija(izvorniQString,iterator);
    postaviOK(pomocna.compare("1")==0?1:0);

    return iterator;
}
