#ifndef ODGOVOR_OBRISAN_H
#define ODGOVOR_OBRISAN_H

#include <QObject>
#include "Engine/Enums.h"
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"
#include "Engine/fje_za_koriscenje.h"

class Odgovor_Obrisan : public Odgovor_na_Komandu
{
    Q_OBJECT
public:
    Odgovor_Obrisan();
    ~Odgovor_Obrisan();

    void postaviOK(bool novo);
    bool vratiOK();

    QByteArray   za_slanje();
    int          praviOd(QString& izvorniBitArray);
};

#endif // ODGOVOR_OBRISAN_H
