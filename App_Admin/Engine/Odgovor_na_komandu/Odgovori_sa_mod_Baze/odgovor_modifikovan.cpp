#include "Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_modifikovan.h"

Odgovor_Modifikovan::Odgovor_Modifikovan()
{
    Tip=ODGOVOR_MODIFIKOVAN;
}
Odgovor_Modifikovan::~Odgovor_Modifikovan()
{

}

void Odgovor_Modifikovan::postaviOK(bool novo)
{
    OK=novo;
}

bool Odgovor_Modifikovan::vratiOK()
{
    return OK;
}

QByteArray  Odgovor_Modifikovan::za_slanje()
{
    // TIP || OK
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(QString::number(OK?1:0)));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}

int Odgovor_Modifikovan::praviOd(QString& izvorniQString)
{

    // TIP || OK
        using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=ODGOVOR_MODIFIKOVAN)
        return 0;

    //dekapsulacija
    Tip=ODGOVOR_MODIFIKOVAN;
    pomocna=dekapsulacija(izvorniQString,iterator);
    postaviOK(pomocna.compare("1")==0?1:0);

    return iterator;
}
