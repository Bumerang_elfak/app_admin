#ifndef ODGOVOR_PREDMETI_H
#define ODGOVOR_PREDMETI_H

#include <QObject>
#include <QString>
#include <QList>
#include "Engine/Enums.h"
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"
#include "Engine/fje_za_koriscenje.h"

#include "Engine/PPOK/predmet.h"
#include "Engine/PPOK/ppok.h"
#include "Engine/PPOK/pitanje.h"

class Odgovor_Predmeti : public Odgovor_na_Komandu
{
    Q_OBJECT
public:
    Odgovor_Predmeti();
    ~Odgovor_Predmeti();

    QList<Predmet> vratiListu()    {return listaPredmeta;}

    void dodaj_predmet(Predmet &p){
        listaPredmeta.append(p);
    }
    void brisi_predmet(Predmet &p){
        int i=listaPredmeta.indexOf(p);
        if( i==-1 )
            return;
        listaPredmeta.removeAt(i);
    }
    void dodaj_listu_predmeta(QList<Predmet> p){
        listaPredmeta.append(p);
    }

    QByteArray  za_slanje();
    int praviOd(QString& a);

private:
    QList<Predmet> listaPredmeta;
};

#endif // ODGOVOR_PREDMETI_H
