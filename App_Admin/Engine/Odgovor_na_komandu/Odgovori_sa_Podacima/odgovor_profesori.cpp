#include "Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_profesori.h"

Odgovor_Profesori::Odgovor_Profesori()
{
    Tip=ODGOVOR_PROFESORI;
}
Odgovor_Profesori::~Odgovor_Profesori()
{
    listaProfesora.clear();
}

QByteArray Odgovor_Profesori::za_slanje()
{
    // tip || br_p || profesori
    using namespace Fje_za_Koriscenje;

    QString stringZaSlanje;
    stringZaSlanje.append(enkapsuliraj(QString::number(Tip)));
    int br_p=listaProfesora.length();
    stringZaSlanje.append(enkapsuliraj(QString::number(br_p)));

    for( int i=0; i<br_p; i++ )
        stringZaSlanje.append(enkapsuliraj(listaProfesora[i].za_slanje()));

    QByteArray polje_za_slanje;
    polje_za_slanje.append(stringZaSlanje);
    return polje_za_slanje;
}

int Odgovor_Profesori::praviOd(QString& s)
{
    // tip || br_p || profesori
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;

    pomocna=dekapsulacija(s,iterator);
    if( pomocna.toInt()!=ODGOVOR_PROFESORI)
        return 0;
    Tip=ODGOVOR_PROFESORI;

    pomocna=dekapsulacija(s,iterator);
    int br_p=pomocna.toInt();

    for( int i=0; i<br_p; i++ )
    {
        Profesor k;
        QString ba;
        ba.append(dekapsulacija(s,iterator));
        k.pravi_od(ba);
        listaProfesora.insert(0,k);
    }
    return iterator;
}
