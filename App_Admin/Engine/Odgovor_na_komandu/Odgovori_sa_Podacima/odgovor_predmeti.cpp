#include "Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_predmeti.h"

Odgovor_Predmeti::Odgovor_Predmeti()
{
    Tip=ODGOVOR_PREDMETI;
}

Odgovor_Predmeti::~Odgovor_Predmeti()
{
    listaPredmeta.clear();
}


QByteArray Odgovor_Predmeti::za_slanje()
{
    // tip || br_p || predmeti
    using namespace Fje_za_Koriscenje;

    QString stringZaSlanje;
    stringZaSlanje.append(enkapsuliraj(QString::number(Tip)));
    int br_p=listaPredmeta.length();
    stringZaSlanje.append(enkapsuliraj(QString::number(br_p)));

    for( int i=0; i<br_p; i++ )
        stringZaSlanje.append(enkapsuliraj(listaPredmeta[i].za_slanje()));

    QByteArray polje_za_slanje;
    polje_za_slanje.append(stringZaSlanje);
    return polje_za_slanje;
}

int Odgovor_Predmeti::praviOd(QString& s)
{
    // tip || br_p || predmeti
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;

    pomocna=dekapsulacija(s,iterator);
    if( pomocna.toInt()!=ODGOVOR_PREDMETI)
        return 0;
    Tip=ODGOVOR_PREDMETI;

    pomocna=dekapsulacija(s,iterator);
    int br_p=pomocna.toInt();

    for( int i=0; i<br_p; i++ )
    {
        Predmet k;
        QString ba;
        ba.append(dekapsulacija(s,iterator));
        k.pravi_od(ba);
        listaPredmeta.insert(0,k);
    }
    return iterator;
}
