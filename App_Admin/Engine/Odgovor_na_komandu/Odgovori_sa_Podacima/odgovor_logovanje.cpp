#include "odgovor_logovanje.h"

odgovor_logovanje::odgovor_logovanje()
{
    Tip=ODGOVOR_LOGOVANJE;
}

odgovor_logovanje::~odgovor_logovanje()
{
    delete korisnik;
}

odgovor_logovanje::odgovor_logovanje(Tipovi tipOsobe)
{
    Tip=ODGOVOR_LOGOVANJE;
    this->pravi_korisnika(tipOsobe);
}

QByteArray odgovor_logovanje::za_slanje()
{
    // tip || da_li_korisnik_posotji || korisnik
    using namespace Fje_za_Koriscenje;

    QString stringZaSlanje;
    stringZaSlanje.append(enkapsuliraj(QString::number(Tip)));
    if( korisnik!=NULL ){
        //E korisnik
        stringZaSlanje.append(enkapsuliraj(QString::number(1)));//E korisnik
        stringZaSlanje.append(enkapsuliraj(korisnik->za_slanje()));
    }
    else
        stringZaSlanje.append(enkapsuliraj(QString::number(0)));//ne E korisnik

    QByteArray polje_za_slanje;
    polje_za_slanje.append(stringZaSlanje);
    return polje_za_slanje;
}

int odgovor_logovanje::praviOd(QString& izvorniStringArray)
{
    // tip || da_li_korisnik_posotji || korisnik
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniStringArray,iterator);

    if( pomocna.toInt()!=ODGOVOR_LOGOVANJE){
        korisnik=NULL;
        return 0;
    }

    pomocna=dekapsulacija(izvorniStringArray,iterator);
    if( pomocna=="1" ){
        //korisnik postoji
        pomocna=dekapsulacija(izvorniStringArray,iterator);//ceo korisnik
        QString dodatna;
        int n=0;
        dodatna=dekapsulacija(pomocna,n);
        n=dodatna.toInt();//u n je tip Korisnika
        switch(n){
        case ADMIN:{
            korisnik=new Admin();
            korisnik->pravi_od(pomocna);
            break;
        }
        case PROFESOR:{
            korisnik=new Profesor();
            korisnik->pravi_od(pomocna);
            break;
        }
        case STUDENT:{
            korisnik=new Student();
            korisnik->pravi_od(pomocna);
            break;
        }
        }
    }
    else{
        //korisnik ne postoji
        korisnik=NULL;
    }

    return iterator;
}


