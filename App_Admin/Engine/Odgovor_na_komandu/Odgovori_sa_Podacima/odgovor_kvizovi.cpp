#include "Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_kvizovi.h"

Odgovor_Kvizovi::Odgovor_Kvizovi()
{
    Tip=ODGOVOR_KVIZOVI;
}

Odgovor_Kvizovi::~Odgovor_Kvizovi()
{
    listaKvizova.clear();
}

QByteArray Odgovor_Kvizovi::za_slanje()
{
    // tip || br_k || kvizovi
    using namespace Fje_za_Koriscenje;

    QString stringZaSlanje;
    stringZaSlanje.append(enkapsuliraj(QString::number(Tip)));
    int br_k=listaKvizova.length();
    stringZaSlanje.append(enkapsuliraj(QString::number(br_k)));

    for( int i=0; i<br_k; i++ )
        stringZaSlanje.append(enkapsuliraj(listaKvizova[i].za_slanje()));

    QByteArray polje_za_slanje;
    polje_za_slanje.append(stringZaSlanje);
    return polje_za_slanje;
}

int Odgovor_Kvizovi::praviOd(QString& izvorniBitArray)
{
    // tip || br_k || kvizovi
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;


    pomocna=dekapsulacija(izvorniBitArray,iterator);
    if( pomocna.toInt()!=ODGOVOR_KVIZOVI)
        return 0;
    Tip=ODGOVOR_KVIZOVI;

    pomocna=dekapsulacija(izvorniBitArray,iterator);
    int br_k=pomocna.toInt();

    for( int i=0; i<br_k; i++ )
    {
        Kviz k;
        QString ba;
        ba.append(dekapsulacija(izvorniBitArray,iterator));
        k.pravi_od(ba);
        listaKvizova.insert(0,k);
    }
    return iterator;
}

