#ifndef ODGOVOR_LOGOVANJE_H
#define ODGOVOR_LOGOVANJE_H

#include <QObject>
#include <QString>
#include <QList>

#include "Engine/Enums.h"
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"
#include "Engine/fje_za_koriscenje.h"

#include "Engine/Osobe/osobe.h"
#include "Engine/Osobe/admin.h"
#include "Engine/Osobe/student.h"
#include "Engine/Osobe/profesor.h"
#include "Engine/PPOK/ppok.h"

class odgovor_logovanje : public Odgovor_na_Komandu
{
    Q_OBJECT
    //kada se obrise odgovor brise se i korisnik koga je nosio
    //nemojte da zaboravite ovo se radi u destruktoru pa ako je za app
    //prakticnije da se ne brise onda samo sklonite liniju iz destruktora!
public:
    odgovor_logovanje();
    odgovor_logovanje(Tipovi tipOsobe);
    ~odgovor_logovanje();

    void pravi_korisnika(Tipovi T){
        switch(T){
        case ADMIN:{
            korisnik=new Admin();
            break;
        }
        case PROFESOR:{
            korisnik=new Profesor();
            break;
        }
        case STUDENT:{
            korisnik=new Student();
            break;
        }
        default:{
            //xD
            korisnik=NULL;
        }
        }
    }

    void promeniId(int novo){
        if( korisnik!=NULL ){
            korisnik->promeniId(novo);
        }
    }
    void promeniIme(QString novo){
        if( korisnik!=NULL ){
            korisnik->promeniIme(novo);
        }
    }
    void promeniPrezime(QString novo){
        if( korisnik!=NULL ){
            korisnik->promeniPrezime(novo);
        }
    }
    void promeniEmail(QString novo){
        if( korisnik!=NULL ){
            korisnik->promeniEmail(novo);
        }
    }
    void promeniUsername(QString novo){
        if( korisnik!=NULL ){
            korisnik->promeniUsername(novo);
        }
    }
    void promeniPassword(QString novo){
        if( korisnik!=NULL ){
            korisnik->promeniPassword(novo);
        }
    }


    int vratiId(){
        return korisnik->vrati_ID();
    }
    QString vratiIme(){
        return korisnik->vrati_Ime();
    }
    QString vratiPrezime(){
        return korisnik->vrati_Prezime();
    }
    QString vratiEmail(){
        return korisnik->vrati_Email();
    }
    QString vratiUsername(){
        return korisnik->vrati_Username();
    }
    QString vrati_Password(){
        return korisnik->vrati_Password();
    }
    int vrati_tip(){
        return korisnik->vrati_tip();
    }
    Osobe* vrati_korisnika(){
        return korisnik;
    }



    QByteArray  za_slanje();
    int praviOd(QString& izvorniBitArray);

private:
    Osobe *korisnik=NULL;

};

#endif // ODGOVOR_LOGOVANJE_H
