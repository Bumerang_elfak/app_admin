#include "Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_oblasti.h"

Odgovor_Oblasti::Odgovor_Oblasti()
{
    Tip=ODGOVOR_OBLASTI;
}
Odgovor_Oblasti::~Odgovor_Oblasti()
{
    listaOblasti.clear();
}

QByteArray Odgovor_Oblasti::za_slanje()
{
    // tip || br_o || oblasti
    using namespace Fje_za_Koriscenje;

    QString stringZaSlanje;
    stringZaSlanje.append(enkapsuliraj(QString::number(Tip)));
    int br_o=listaOblasti.length();
    stringZaSlanje.append(enkapsuliraj(QString::number(br_o)));

    for( int i=0; i<br_o; i++ )
        stringZaSlanje.append(enkapsuliraj(listaOblasti[i].za_slanje()));

    QByteArray polje_za_slanje;
    polje_za_slanje.append(stringZaSlanje);
    return polje_za_slanje;
}

int Odgovor_Oblasti::praviOd(QString& izvorniBitArray)
{
    // tip || br_k || kvizovi
    using namespace Fje_za_Koriscenje;


    int iterator=0;
    QString pomocna;

    pomocna=dekapsulacija(izvorniBitArray,iterator);
    if( pomocna.toInt()!=ODGOVOR_OBLASTI)
        return 0;
    Tip=ODGOVOR_OBLASTI;

    pomocna=dekapsulacija(izvorniBitArray,iterator);
    int br_o=pomocna.toInt();

    for( int i=0; i<br_o; i++ )
    {
        Oblast k;
        QString ba;
        ba.append(dekapsulacija(izvorniBitArray,iterator));
        k.pravi_od(ba);
        listaOblasti.insert(0,k);
    }
    return iterator;
}
