#ifndef ERROR_H
#define ERROR_H

#include <QObject>
#include "Engine/Enums.h"
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"

class Error : public Odgovor_na_Komandu
{
    Q_OBJECT
public:
    QString greska;
    Error();

    void postavi_greska(QString g){
        greska=g;
    }
    QString vrati_greska(){
        return greska;
    }

    virtual QByteArray  za_slanje();
    virtual int         praviOd(QString& s);
};

#endif // ERROR_H
