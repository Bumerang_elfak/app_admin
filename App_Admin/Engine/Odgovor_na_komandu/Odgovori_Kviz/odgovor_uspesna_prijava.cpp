#include "Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_uspesna_prijava.h"

Odgovor_Uspesna_Prijava::Odgovor_Uspesna_Prijava(){
    Tip=ODGOVOR_KVIZ_USPESNA_PRIJAVA;
}
QByteArray  Odgovor_Uspesna_Prijava::za_slanje(){

    // TIP || KVIZ
    using namespace Fje_za_Koriscenje;

    QString s;
    s.append(enkapsuliraj(QString::number(Tip)));
    QString kviz="";
    kviz.append(kviz_koji_se_radi.za_slanje());
    s.append(enkapsuliraj(kviz));
    QByteArray ba;
    ba.append(s);
    return ba;

}
int        Odgovor_Uspesna_Prijava::praviOd(QString& s){
        //TIP || KVIZ
        using namespace Fje_za_Koriscenje;

        int iterator=0;
        QString pomocna;

        pomocna=dekapsulacija(s,iterator);
        if(pomocna.toInt()!=ODGOVOR_KVIZ_USPESNA_PRIJAVA )
            return 0;


        pomocna=dekapsulacija(s,iterator);
        kviz_koji_se_radi.pravi_od(pomocna);
        return iterator;//OVO POGLEDATI PONOVO TREBALO BI OVAKO DA BUDE
}
