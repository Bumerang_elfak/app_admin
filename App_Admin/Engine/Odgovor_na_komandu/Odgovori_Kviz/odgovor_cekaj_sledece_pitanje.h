#ifndef ODGOVOR_CEKAJ_SLEDECE_PITANJE_H
#define ODGOVOR_CEKAJ_SLEDECE_PITANJE_H

#include <QObject>
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"
#include "Engine/Enums.h"

class odgovor_cekaj_sledece_pitanje : public Odgovor_na_Komandu
{
    Q_OBJECT

public:
    odgovor_cekaj_sledece_pitanje();

    virtual QByteArray  za_slanje();
    virtual int        praviOd(QString& izvorniBitArray);

};

#endif // ODGOVOR_CEKAJ_SLEDECE_PITANJE_H
