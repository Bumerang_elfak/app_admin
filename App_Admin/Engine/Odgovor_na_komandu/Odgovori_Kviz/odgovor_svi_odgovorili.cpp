#include "Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_svi_odgovorili.h"

Odgovor_Svi_Odgovorili::Odgovor_Svi_Odgovorili(){
    Tip=ODGOVOR_KVIZ_SVI_ODGOVORILI;
}
QByteArray  Odgovor_Svi_Odgovorili::za_slanje(){
    //TIP
    using namespace Fje_za_Koriscenje;

    QString s;
    s.append(enkapsuliraj(QString::number(Tip)));
    QByteArray ba;
    ba.append(s);
    return ba;
}
int        Odgovor_Svi_Odgovorili::praviOd(QString& s){
    //TIP
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;

    pomocna=dekapsulacija(s,iterator);
    if(pomocna.toInt()!=ODGOVOR_KVIZ_SVI_ODGOVORILI )
        return 0;
    return iterator;
}
