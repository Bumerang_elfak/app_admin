#ifndef ODGOVOR_TREN_STATISTIKA_PITANJA_H
#define ODGOVOR_TREN_STATISTIKA_PITANJA_H

#include <QObject>
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"

class Odgovor_tren_statistika_pitanja : public Odgovor_na_Komandu
{
    Q_OBJECT
    int br_tacnih=0;
    int br_ne_tacnih=0;
    int ukupno=0;

public:
    Odgovor_tren_statistika_pitanja();

    void postavi_br_tacnih(int br){
        br_tacnih=br;
    }
    void postavi_br_ne_tacnih(int br){
        br_ne_tacnih=br;
    }
    void postavi_ukupno(int br){
        ukupno=br;
    }

    int vrati_br_tacnih(){
        return br_tacnih;
    }
    int vrati_br_ne_tacnih(){
        return br_ne_tacnih;
    }
    int vrati_ukupno(){
        return ukupno;
    }

    int vrati_procenat_tacnih(){
        if( ukupno!=0 )
            return br_tacnih*100/ukupno;
        else
            return 0;
    }
    int vrati_procenat_ne_tacnih(){
        if( ukupno!=0 )
            return br_ne_tacnih*100/ukupno;
        else
            return 0;
    }

    virtual QByteArray  za_slanje();
    virtual int         praviOd(QString& izvorniBitArray);
};

#endif // ODGOVOR_TREN_STATISTIKA_PITANJA_H
