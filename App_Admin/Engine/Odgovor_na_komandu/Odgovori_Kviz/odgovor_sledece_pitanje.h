#ifndef ODGOVOR_SLEDECE_PITANJE_H
#define ODGOVOR_SLEDECE_PITANJE_H

#include <QObject>
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"

class Odgovor_sledece_Pitanje : public Odgovor_na_Komandu
{
    Q_OBJECT
    int sledece_pitanje=-1;
public:
    Odgovor_sledece_Pitanje();

    int vrati_sledece_pitanje(){
        return sledece_pitanje;
    }
    void postavi_sledece_pitanje(int i){
        sledece_pitanje=i;
    }
    virtual QByteArray  za_slanje();
    virtual int         praviOd(QString& izvorniBitArray);
};

#endif // ODGOVOR_SLEDECE_PITANJE_H
