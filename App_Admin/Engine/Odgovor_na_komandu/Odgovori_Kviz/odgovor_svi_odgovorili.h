#ifndef ODGOVOR_SVI_ODGOVORILI_H
#define ODGOVOR_SVI_ODGOVORILI_H

#include <QObject>
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"

class Odgovor_Svi_Odgovorili : public Odgovor_na_Komandu
{
    Q_OBJECT
public:
    Odgovor_Svi_Odgovorili();

    virtual QByteArray  za_slanje();
    virtual int         praviOd(QString& izvorniBitArray);
};

#endif // ODGOVOR_SVI_ODGOVORILI_H
