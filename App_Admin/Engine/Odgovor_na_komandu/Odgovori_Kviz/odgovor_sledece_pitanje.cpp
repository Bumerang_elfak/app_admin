#include "Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_sledece_pitanje.h"

Odgovor_sledece_Pitanje::Odgovor_sledece_Pitanje(){
    Tip=ODGOVOR_KVIZ_SLEDECE_PITANJE;
}
QByteArray  Odgovor_sledece_Pitanje::za_slanje(){
    //TIP || sledece_pitanje
    using namespace Fje_za_Koriscenje;

    QString s;
    s.append(enkapsuliraj(QString::number(Tip)));
    s.append(enkapsuliraj(QString::number(sledece_pitanje)));
    QByteArray ba;
    ba.append(s);
    return ba;
}
int         Odgovor_sledece_Pitanje::praviOd(QString& s){
    //TIP
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;

    pomocna=dekapsulacija(s,iterator);
    if(pomocna.toInt()!=ODGOVOR_KVIZ_SLEDECE_PITANJE )
        return 0;
    pomocna=dekapsulacija(s,iterator);
    sledece_pitanje=pomocna.toInt();
    return iterator;
}
