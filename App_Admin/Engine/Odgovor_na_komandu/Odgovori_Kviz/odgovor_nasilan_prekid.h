#ifndef ODGOVOR_NASILAN_PREKID_H
#define ODGOVOR_NASILAN_PREKID_H

#include <QObject>
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"

class Odgovor_nasilan_Prekid : public Odgovor_na_Komandu
{

public:
    Odgovor_nasilan_Prekid();

    virtual QByteArray  za_slanje();
    virtual int         praviOd(QString& s);
};

#endif // ODGOVOR_NASILAN_PREKID_H
