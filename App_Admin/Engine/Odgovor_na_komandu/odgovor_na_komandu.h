#ifndef ODGOVOR_NA_KOMANDU_H
#define ODGOVOR_NA_KOMANDU_H

#include <QObject>
#include <QTcpSocket>
#include "Engine/fje_za_koriscenje.h"
#include "Engine/Enums.h"
#include <iostream>

class Odgovor_na_Komandu : public QObject
{
    Q_OBJECT
protected:
    bool OK=true;
    QTcpSocket* Adresa_povratka=NULL;
    Tipovi Tip;


public:
    explicit Odgovor_na_Komandu(QObject *parent = 0);
    virtual ~Odgovor_na_Komandu();

    void postavi_adresu_povratka(QTcpSocket* A){
        Adresa_povratka=A;
    }
    QTcpSocket* vrati_adr_povratka(){
        return Adresa_povratka;
    }
    Tipovi vrati_tip(){
        return Tip;
    }
    bool isOk(){
        return OK;
    }
    void salji_na(QTcpSocket* sock);
    virtual QByteArray  za_slanje()=0;
    virtual int         praviOd(QString& izvorniBitArray)=0;


};

#endif // ODGOVOR_NA_KOMANDU_H
