#include "komunikacija_sa_serverom.h"



Komunikacija_sa_Serverom *Komunikacija_sa_Serverom::obj=NULL;
Komunikacija_sa_Serverom::Komunikacija_sa_Serverom(){
    Komunikacija_sa_serverom_Worker *objekat_workera=new Komunikacija_sa_serverom_Worker();
    objekat_workera->moveToThread(&Worker);
    connect(&Worker,SIGNAL(finished()), objekat_workera, SLOT(deleteLater()));
    connect(this, SIGNAL(salji_workeru(Komanda*)),
            objekat_workera, SLOT(salji_komandu(Komanda*)),
            Qt::QueuedConnection);
    connect(objekat_workera,SIGNAL(stigo_odg(Odgovor_na_Komandu*)),
            this,SLOT(prihvati_od_workera(Odgovor_na_Komandu*)),
            Qt::QueuedConnection);
    connect(&Worker, SIGNAL(started()),
            this,SLOT(slot_worker_run()),Qt::QueuedConnection);
    connect(&Worker,SIGNAL(finished()),
            this,SLOT(slot_worker_end()),Qt::QueuedConnection);
    connect(this,SIGNAL(pravi_socket()),
            objekat_workera,SLOT(slot_pravi_soket()),
            Qt::QueuedConnection);
    Worker.start();
    emit pravi_socket();
}
Komunikacija_sa_Serverom::~Komunikacija_sa_Serverom(){
}


void Komunikacija_sa_Serverom::salji_komandu( Komanda* k ){
    emit salji_workeru(k);
}


void Komunikacija_sa_Serverom::prihvati_od_workera( Odgovor_na_Komandu* odg){
    emit stigo_odg(odg);
}

void Komunikacija_sa_Serverom::slot_worker_run(){
    std::cout<<"THREAD POCEO RADI"<<std::endl;
}
void Komunikacija_sa_Serverom::slot_worker_end(){
    std::cout<<"THREAD PRESTAO DA RADI CEKAMO OBJEKAT DUMRE"<<std::endl;
}
