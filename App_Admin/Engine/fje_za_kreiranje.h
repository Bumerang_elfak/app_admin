#ifndef FJE_ZA_KREIRANJE_H
#define FJE_ZA_KREIRANJE_H

//KOMANDE
#include "Engine/Komanda/komanda_generisi_kviz_dinamicki.h"
#include "Engine/Komanda/komanda_logovanje.h"
#include "Engine/Komanda/komanda_vrati_sve_aktivne_kvizove.h"
#include "Engine/Komanda/komanda_vrati_sve_logovane_profesore.h"

//KOMANDA_KVIZ
#include "Engine/Komanda/Komanda_Kviz/komanda_isteklo_vreme.h"
#include "Engine/Komanda/Komanda_Kviz/komanda_kviz_aktiviraj.h"
#include "Engine/Komanda/Komanda_Kviz/komanda_kviz_log.h"
#include "Engine/Komanda/Komanda_Kviz/komanda_kviz_nasilno_prekini.h"
#include "Engine/Komanda/Komanda_Kviz/komanda_kviz_normalan_prekid.h"
#include "Engine/Komanda/Komanda_Kviz/komanda_kviz_odg_pitanje.h"
#include "Engine/Komanda/Komanda_Kviz/komanda_kviz_prikazi_statistiku.h"
#include "Engine/Komanda/Komanda_Kviz/komanda_kviz_sl_pitanje.h"

//KOMANDA_NAD_BAZOM
#include "Engine/Komanda/Komanda_nad_bazom/komandabrisi.h"

#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_admin.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_kviz.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_oblast.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_pitanje.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_poseduje_pitanje.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_predmet.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_profesor.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_profesor.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_student.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_ceo_kviz.h"

#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komanda_modifikuj_celo_pitanje.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komanda_modifikuj_ceo_kviz.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_admin.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_kviz.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_oblast.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_pitanje.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_predmet.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_profesor.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_student.h"

#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_admin.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_kviz.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_kvizovi_za_pitanja.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_oblast.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_pitanje.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_predmet.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_profesor.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_student.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_profesori_bez_predmeta.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_pitanja_za_kviz.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_sve_za_profesora.h"


//ODGOVORI
#include "Engine/Odgovor_na_komandu/error.h"
#include "Engine/Odgovor_na_komandu/error_diskonektovan_sa_servera.h"
//ODGOVOR_KVIZ
#include "Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_aktivan_kviz.h"
#include "Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_isteklo_vreme.h"
#include "Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_sledece_pitanje.h"
#include "Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_svi_odgovorili.h"
#include "Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_tren_statistika_pitanja.h"
#include "Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_uspesna_prijava.h"
#include "Engine/Odgovor_na_komandu/Odgovori_Kviz/Odgovor_Uspesno_zavrsen_Kviz.h"
#include "Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_nasilan_prekid.h"
#include "Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_cekaj_sledece_pitanje.h"
//ODGOVORI_SA_MOD_BAZE
#include "Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_modifikovan.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_obrisan.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_snimljen.h"
//ODGOVORI_SA_PODACIMA
#include "Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_kvizovi.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_oblasti.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_pitanja.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_predmeti.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_profesori.h"

#include "Engine/fje_za_koriscenje.h"

namespace Fje_za_kreiranje_objekata{

Komanda*            pravi_Komandu(QString s);
Odgovor_na_Komandu* pravi_Odgovor_na_komandu(QString s);

}
#endif // FJE_ZA_KREIRANJE_H
