#include "Engine/fje_za_koriscenje.h"

namespace Fje_za_Koriscenje{

int vrati_broj(QString s, int &broj){
    QString ns;
    int suz_s=s.length();
    if( !s[broj].isDigit() )
        return 0;
    while( s[broj].isDigit() && suz_s>=broj ){
        ns.append(s[broj]);
        broj++;
    }
    return ns.toInt();
}
int broj_cifara_broja(int br){
    int br_cr=0;
    while(br!=0){
        br_cr++;
        br=br / 10;
    }
    return br_cr;
}
QString dekapsulacija( QString s, int &broj ){
    if(s[broj]!='<')
        return QString();
    QString string;
    broj++;
    while( s[broj]!='>' ){
        if( s[broj]==QChar('/') )
            broj++;
        string.append(s[broj++]);
    }
    broj++;
    return string;
}
QString enkapsuliraj(QString s){
    QString string;
    string.append("<");
    int n=s.length();
    for( int i=0; i<n; i++ ){
        string.append(s[i]);
        if( s[i+1]=='>' || s[i+1]=='/' )
            string.append('/');
    }
    string.append(">");
    return string;
}

QString enkapsulirajZaBazu(QString s)
{
    if(s=="")
    {
        return "";
    }
    return "\'"+s+"\'";
}

QString enkapsulirajZaBazu(int s)
{
    return QString::number(s);
}

QString enkapsulirajZaBazu(bool s)
{
    if( s )
        return QString::number(1);
    else
        return QString::number(0);
}

}
