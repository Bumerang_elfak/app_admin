#ifndef STUDENT_H
#define STUDENT_H

#include "Engine/Osobe/osobe.h"
#include "Engine/fje_za_koriscenje.h"
#include <QString>

class Student : public Osobe
{
    Q_OBJECT
public:
    Student();
    Student(const Student& student);
    ~Student();
    Student(int idStudenta, QString username, QString password);


    Student& operator=(const Student& student);
    bool operator==(const Student& student);

    QByteArray za_slanje();
    int pravi_od(QString &izvorniBitArray);

private:

};

#endif // STUDENT_H
