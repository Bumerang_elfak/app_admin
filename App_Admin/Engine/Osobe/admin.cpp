#include "Engine/Osobe/admin.h"

Admin::Admin(QObject *parent)
    :Osobe(parent)
{
    tipOsobe=ADMIN;
}
Admin::Admin(const Admin &admin)
    :Osobe(admin)
{
    Admin();
    Id=admin.Id;
    Ime=admin.Ime;
    Prezime=admin.Prezime;
    E_mail=admin.E_mail;
    Username=admin.Username;
    Password=admin.Password;
}

Admin::~Admin()
{

}
Admin:: Admin(int idadmina, QString ime,QString prezime, QString email, QString username, QString password)
{
    Id=idadmina;
    Ime=ime;
    Prezime=prezime;
    E_mail=email;
    Username=username;
    Password=password;
}

void Admin::promeniIme(QString novo)
{
    Ime=novo;
}
void Admin::promeniPrezime(QString novo)
{
    Prezime=novo;
}
void Admin::promeniEmail(QString novo)
{
    E_mail=novo;
}


QByteArray Admin::za_slanje()
{
    // tipOsobe || ID || ime || Prezime || E_mail || Username || Password
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(tipOsobe)));
    povratniString.append(enkapsuliraj(QString::number(Id)));
    povratniString.append(enkapsuliraj(Ime));
    povratniString.append(enkapsuliraj(Prezime));
    povratniString.append(enkapsuliraj(E_mail));
    povratniString.append(enkapsuliraj(Username));
    povratniString.append(enkapsuliraj(Password));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}

int Admin::pravi_od(QString &izvorniQString)
{
    using namespace Fje_za_Koriscenje;
    // tipOsobe || ID || ime || Prezime || E_mail || Username || Password

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=ADMIN)
        return 0;

    //dekapsulacija
    tipOsobe=ADMIN;
    pomocna=dekapsulacija(izvorniQString,iterator);
    Id=pomocna.toInt();
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIme(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniPrezime(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniEmail(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    Username=pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    Password=pomocna;

    return iterator;
}

Admin& Admin::operator=(const Admin& admin)
{
    Id=admin.Id;
    Ime=admin.Ime;
    Prezime=admin.Prezime;
    E_mail=admin.E_mail;
    Username=admin.Username;
    Password=admin.Password;
    return *this;
}

bool Admin::operator==(const Admin& admin)
{
    if(Id!=admin.Id)
        return false;
    if(Ime.compare(admin.Ime)==0)
        return false;
    if(Prezime.compare(admin.Prezime)==0)
        return false;
    if(E_mail.compare(admin.E_mail)==0)
        return false;
    if(Username.compare(admin.Username)==0)
        return false;
    if(Password.compare(admin.Password)==0)
        return false;

    return true;
}
