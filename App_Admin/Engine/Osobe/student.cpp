#include "Engine/Osobe/student.h"

Student::Student()
{
    tipOsobe=STUDENT;
}
Student::Student(const Student &student):Osobe(student)
{
    tipOsobe=STUDENT;
    Id=student.Id;
    Username=student.Username;
    Password=student.Password;
}
Student::~Student()
{

}

Student::Student(int idstudenta, QString username, QString password)
{
    Id=idstudenta;
    Username=username;
    Password=password;
}


QByteArray Student::za_slanje()
{
    // tipOsobe || ID || Username || Password
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(tipOsobe)));
    povratniString.append(enkapsuliraj(QString::number(Id)));
    povratniString.append(enkapsuliraj(Username));
    povratniString.append(enkapsuliraj(Password));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}

int Student::pravi_od(QString &izvorniQString)
{
    using namespace Fje_za_Koriscenje;
    // tipOsobe || ID || Username || Password

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=STUDENT)
        return 0;

    //dekapsulacija
    tipOsobe=STUDENT;
    pomocna=dekapsulacija(izvorniQString,iterator);
    Id=pomocna.toInt();
    pomocna=dekapsulacija(izvorniQString,iterator);
    Username=pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    Password=pomocna;

    return iterator;
}

Student& Student::operator=(const Student& student)
{
    Id=student.Id;
    Username=student.Username;
    Password=student.Password;
    return *this;
}

bool Student::operator==(const Student& student)
{
    if(Id!=student.Id)
        return false;
    if(Username.compare(student.Username)==0)
        return false;
    if(Password.compare(student.Password)==0)
        return false;

    return true;
}
