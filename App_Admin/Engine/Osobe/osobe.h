#ifndef OSOBE_H
#define OSOBE_H

#include <QObject>
#include <QTcpSocket>
#include "Engine/Enums.h"
#include "Engine/fje_za_koriscenje.h"


class Osobe : public QObject
{
    Q_OBJECT
public:
    explicit Osobe(QObject *parent = 0);
    Osobe(const Osobe& o);

    virtual ~Osobe();
    virtual QByteArray za_slanje()=0;
    virtual int pravi_od(QString &izvorniBitArray)=0;

    void promeniId( int ID){
        Id=ID;
    }
    void promeniUsername( QString user ){
        Username=user;//xD USER
    }
    void promeniPassword( QString pass ){
        Password=pass;
    }
    virtual void promeniIme(QString novo){
        novo.append("");
    }
    virtual void promeniPrezime(QString novo){
        novo.append("");
    }
    virtual void promeniEmail(QString novo){
        novo.append("");
    }


    int vrati_ID(){
        return Id;
    }
    QString vrati_Username(){
        return Username;
    }
    QString vrati_Password(){
        return Password;
    }
    virtual QString vrati_Ime(){
        return "";
    }
    virtual QString vrati_Prezime(){
        return "";
    }
    virtual QString vrati_Email(){
        return "";
    }
    void postavi_sock( QTcpSocket *s ){
        sock=s;
    }
    QTcpSocket* vrati_sock(){
        return sock;
    }

    void    salji_na_port(QTcpSocket *sock){
        sock->write(za_slanje());
    }
    int     vrati_tip(){
        return tipOsobe;
    }
protected:

    Tipovi tipOsobe;
    int Id;
    QTcpSocket *sock;
    QString Username;
    QString Password;
};

#endif // OSOBE_H
