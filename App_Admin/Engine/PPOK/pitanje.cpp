#include "Engine/PPOK/pitanje.h"

//constrictors & destructors
Pitanje::Pitanje()
    :PPOK()
{
    br_tren_odg=0;
    tip=PITANJE;
}
Pitanje::Pitanje(const Pitanje& p):
    PPOK(p){
    pravi_od(p);
}
Pitanje::~Pitanje(){
    oduzmi_mem();
}

//functions
//private
void        Pitanje::oduzmi_mem(){
    odgovori.clear();
}
void        Pitanje::pravi_od(const Pitanje& p){
    ID_iz_Baze=p.ID_iz_Baze;
    tezina=p.tezina;
    tacan=p.tacan;
    ime=p.ime;
    for( int i=0; i<4; i++ )
        dodaj_odg(p.odgovori.at(i));
    ID_iz_baze_Oblast=p.ID_iz_baze_Oblast;
    ID_iz_baze_Predmet=p.ID_iz_baze_Predmet;
    ID_iz_baze_Profesor=p.ID_iz_baze_Profesor;
}

//public
bool        Pitanje::dodaj_odg(QString odg){
    if( br_tren_odg>=4 )
        return false;
    odgovori.append(odg);
    return true;
}
void        Pitanje::postavi_tacan(int tacan){
    this->tacan= tacan;
}
int         Pitanje::vrati_tacan(){
    return tacan;
}
bool        Pitanje::postavi_tezinu(int tezina){
    this->tezina=tezina;
    return true;
}
int         Pitanje::vrati_tezinu(){
    return tezina;
}
QString     Pitanje::operator[](int i){
    return odgovori.at(i);
}
bool            Pitanje::postavi_tekst(QString t){
    return PPOK::postavi_ime(t);
}
QList<QString>  Pitanje::vrati_odgovore(){
    return odgovori;
}
QString     Pitanje::vrati_tekst(){
    return PPOK::vrati_ime();
}
QByteArray  Pitanje::za_slanje(){

    //tip || ID_iz_Baze  || ID_iz_baze_Profesor || ID_iz_baze_Predmet || ID_iz_baze_Oblast
    //tekst || odgovor[i] || tacan || tezina
    using namespace Fje_za_Koriscenje;

    QString stringZaSlanje;
    //saljemo tip i tekst
    stringZaSlanje.append(enkapsuliraj(QString::number(tip)));
    stringZaSlanje.append(enkapsuliraj(QString::number(ID_iz_Baze)));
    stringZaSlanje.append(enkapsuliraj(QString::number(ID_iz_baze_Profesor)));
    stringZaSlanje.append(enkapsuliraj(QString::number(ID_iz_baze_Predmet)));
    stringZaSlanje.append(enkapsuliraj(QString::number(ID_iz_baze_Oblast)));
    stringZaSlanje.append(enkapsuliraj(ime));


    //saljemo odgovore
    for( int i=0; i<4; i++ ){
        stringZaSlanje.append(enkapsuliraj(odgovori.at(i)));
    }
    //saljemo tacan odg
    stringZaSlanje.append(enkapsuliraj(QString::number(this->tacan)));
    stringZaSlanje.append(enkapsuliraj(QString::number(this->tezina)));

    QByteArray polje_za_slanje;
    polje_za_slanje.append(stringZaSlanje);
    return polje_za_slanje;
}
int         Pitanje::pravi_od(QString &string){

    //tip || ID_iz_Baze || ID_iz_baze_Profesor || ID_iz_baze_Predmet || ID_iz_baze_Oblast
    //tekst || odgovor[i] || tacan || tezina
    using namespace Fje_za_Koriscenje;

    oduzmi_mem();
    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(string,iterator);
    if(pomocna.toInt()!=PITANJE)
        return 0;

    pomocna=dekapsulacija(string,iterator);
    postavi_ID(pomocna.toInt());
    pomocna=dekapsulacija(string,iterator);
    postavi_ID_Profesora(pomocna.toInt());
    pomocna=dekapsulacija(string,iterator);
    postavi_ID_Predmeta(pomocna.toInt());
    pomocna=dekapsulacija(string,iterator);
    postavi_ID_Oblasti(pomocna.toInt());

    pomocna=dekapsulacija(string,iterator);
    postavi_ime(pomocna);

    for( int i=0; i<4; i++ ){
        pomocna=dekapsulacija(string,iterator);
        dodaj_odg(pomocna);
    }
    pomocna=dekapsulacija(string,iterator);
    tacan=pomocna.toInt();
    pomocna=dekapsulacija(string,iterator);
    tezina=pomocna.toFloat();
    return iterator;
}

//operators
bool        Pitanje::operator==(const Pitanje &p){
    if( ID_iz_Baze==p.ID_iz_Baze )
        return true;
    return false;
}
Pitanje&    Pitanje::operator=(const Pitanje &p){
    oduzmi_mem();
    pravi_od(p);
    return *this;
}

