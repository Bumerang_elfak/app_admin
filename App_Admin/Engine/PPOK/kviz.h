#ifndef KVIZ_H
#define KVIZ_H

#include "Engine/PPOK/ppok.h"
#include "Engine/PPOK/pitanje.h"
#include <QList>

//GRESKE POGLEDATI PRILIKOM PROTOKOLA


class Kviz : public PPOK
{

private:
    Q_OBJECT

    QList<Pitanje>      pitanja;
    int                 tezina=100;
    bool                dinamicki;
    int ID_iz_baze_Profesor;
    int ID_iz_baze_Predmet;
    int ID_iz_baze_Oblast;

    void brisi_mem();
    void pravi_od(const Kviz& k);

public:
    Kviz();
    Kviz(const Kviz& k);
    ~Kviz();

    QList<Pitanje>      vrati_pitanja(){
        return pitanja;
    }
    bool                dodaj_pitanje( Pitanje& pit );
    bool                modifikuj_pitanje( Pitanje p ){
        for( int i=0; i<pitanja.count(); i++ ){
            if( pitanja[i].vrati_ID()==p.vrati_ID()){
                pitanja[i].pravi_od_pitanja(p);
                izracunaj_tezinu();
                return true;
            }
        }
        return false;
    }
    bool                izbaci_pitanje( Pitanje& pit ); //PROVERITI OBAVEZNO!!!
    bool                izbaci_pitanje( int id );
    void                promeni_dinamicki( bool d ){
        dinamicki=d;
    }
    bool                vrati_dinamicki(){
        return dinamicki;
    }
    void                dodaj_listu_pitanja(QList<Pitanje> n_p){
        pitanja.append(n_p);
    }
    int                 vrati_tezinu();                 //vraca tezinu pitanja
    void                izracunaj_tezinu();             //racuna i postavlja tez
    Pitanje*            vrati_pointer_na_pitanje( int id ){
        for( int i=0; i<pitanja.count(); i++ )
            if( pitanja[i].vrati_ID()==id )
                return &pitanja[i];
    }
    bool                pravi_od_kviza(Kviz k){
        brisi_mem();
        pravi_od(k);
        return true;
    }
    void                postavi_pitanju_tezinu( int id_pitanja, int tezina ){
        for( int i=0; i<pitanja.count(); i++ )
            if( pitanja[i].vrati_ID()==id_pitanja )
                pitanja[i].postavi_tezinu(tezina);

    }
    void                postavi_tezinu(int t){
        tezina=t;
    }
    void                postavi_ID_Profesora(int noviID)
    {
        ID_iz_baze_Profesor=noviID;
    }
    int                 vrati_ID_Profesora()
    {
        return ID_iz_baze_Profesor;
    }
    void                postavi_ID_Predmeta(int noviID)
    {
        ID_iz_baze_Predmet=noviID;
    }
    int                 vrati_ID_Predmeta()
    {
        return ID_iz_baze_Predmet;
    }
    void                postavi_ID_Oblasti(int noviID)
    {
        ID_iz_baze_Oblast=noviID;
    }
    int                 vrati_ID_Oblasti()
    {
        return ID_iz_baze_Oblast;
    }
    virtual QByteArray  za_slanje();
    virtual int         pravi_od(QString &a);

    Kviz& operator=(const Kviz& k);
    bool operator==(const Kviz& k);

};

#endif // KVIZ_H
