#ifndef OBLAST_H
#define OBLAST_H

//GRESKE POGLEDATI PRILIKOM PROTOKOLA

#include "Engine/PPOK/kviz.h"
#include "Engine/PPOK/ppok.h"
#include "Engine/PPOK/pitanje.h"
#include <QString>
#include <QList>



class Oblast: public PPOK
{

private:
    Q_OBJECT

    QList<Kviz> kvizovi;
    QList<Pitanje> pitanja;
    int ID_iz_baze_Profesor;
    int ID_iz_baze_Predmet;

    void brisi_mem();
    void pravi_od(const Oblast& o);
public:
    Oblast();
    Oblast(const Oblast& o);
    ~Oblast();

    QList<Kviz>     vrati_kvizove(){
        return kvizovi;
    }
    QList<Pitanje>  vrati_pitanja(){
        return pitanja;
    }
    QList<Kviz>*    vrati_pointer_na_kvizove(){
        return &kvizovi;
    }
    QList<Pitanje>* vrati_pointer_na_pitanja(){
        return &pitanja;
    }

    bool dodaj_kviz(const Kviz& k);
    void modifikuj_pitanje( Pitanje p ){
        for( int i=0; i<pitanja.count(); i++ )
            if( pitanja[i].vrati_ID()==p.vrati_ID() ){
                pitanja[i].pravi_od_pitanja(p);
                break;
            }
        for( int i=0; i<kvizovi.count(); i++ )
            kvizovi[i].modifikuj_pitanje(p);
    }
    void modifikuj_kviz(Kviz k){
        for( int i=0; i<kvizovi.count(); i++ )
            if( kvizovi[i].vrati_ID()==k.vrati_ID() ){
                kvizovi[i].pravi_od_kviza(k);
                break;
            }
        for( int i=0; i<k.vrati_pitanja().count(); i++ )
            modifikuj_pitanje(k.vrati_pitanja()[i]);
    }
    bool dodaj_pitanje(const Pitanje &p);
    bool izbaci_kviz(const Kviz& k);
    bool izbaci_pitanje(const Pitanje &p);
    bool brisi_pitanje(int id){
        bool pov;
        for( int i=0; i<pitanja.count(); i++ )
            if( pitanja[i].vrati_ID()==id ){
                pitanja.removeAt(i);
                i--;
                pov=true;
            }
        for( int i=0; i<kvizovi.count(); i++ ){
            pov+=kvizovi[i].izbaci_pitanje(id);
            kvizovi[i].izracunaj_tezinu();
        }
        return true;
    }
    bool brisi_kviz(int id){
        for( int i=0; i<kvizovi.count(); i++ )
            if( kvizovi[i].vrati_ID()==id ){
                kvizovi.removeAt(i);
                i--;
                return true;
            }
        return false;
    }

    void nadovezi_listu_pitanja(QList<Pitanje> n_p){
        pitanja.append(n_p);
    }
    void nadovezi_listu_kvizova(QList<Kviz> n_p){
        kvizovi.append(n_p);
    }
    void postavi_ID_Profesora(int noviID)
    {
        ID_iz_baze_Profesor=noviID;
    }
    int vrati_ID_Profesora()
    {
        return ID_iz_baze_Profesor;
    }
    void postavi_ID_Predmeta(int noviID)
    {
        ID_iz_baze_Predmet=noviID;
    }
    int vrati_ID_Predmeta()
    {
        return ID_iz_baze_Predmet;
    }

    virtual QByteArray      za_slanje();
    virtual int             pravi_od(QString &a);

    bool operator==(const Oblast& o);
    Oblast& operator=(const Oblast& o);
};


#endif // OBLAST_H
