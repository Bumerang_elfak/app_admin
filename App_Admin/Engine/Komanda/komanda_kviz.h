#ifndef KOMANDA_KVIZ_H
#define KOMANDA_KVIZ_H

#include "Engine/Komanda/Komanda.h"
#include <QObject>

class Komanda_Kviz : public Komanda
{
    Q_OBJECT

protected:
    int ID_Aktivan_kviz;
public:
    Komanda_Kviz();
    virtual ~Komanda_Kviz(){}

    void     postavi_ID_Aktivan_kviz( int ID ){
        ID_Aktivan_kviz=ID;
    }
    int      vrati_ID_Aktivan_kviz(){
        return ID_Aktivan_kviz;
    }


    virtual QByteArray za_slanje()=0;
    virtual int        praviOd(QString &izvorniBitArray)=0;

};

#endif // KOMANDA_KVIZ_H
