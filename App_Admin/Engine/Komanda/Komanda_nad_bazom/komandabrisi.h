#ifndef KOMANDABRISI_H
#define KOMANDABRISI_H
#include <QString>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlDatabase>
#include <QObject>
#include "Engine/Komanda/KomandaNadBazom.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_obrisan.h"
#include "Engine/Odgovor_na_komandu/error.h"
#include "Engine/tabele_i_kolone_iz_baze.h"

class KomandaBrisi : public KomandaNadBazom
{
    Q_OBJECT
public:
    KomandaBrisi();
    ~KomandaBrisi();

    void PromeniTabelu(QString novaTabela){tabela=novaTabela;}
    void PromeniUslov(QString ImeIdaUTabeli, int vrednostIdaUTabeli, int poseduje_pitanje_IdPitanja=-1);
    Odgovor_na_Komandu* izvrsiNadBazom(QSqlDatabase &baza);
    QString toQuery();

    QByteArray  za_slanje();
    int        praviOd(QString &izvorniQString);

private:
    QString tabela="";
    QString IdTabele="";
    int vrednostTabele=-1;

    int vrednostIdPitanja=-1; // za poseduje_pitanje

    /*DELETE FROM table_name
WHERE some_column=some_value; */
};

#endif // KOMANDABRISI_H
