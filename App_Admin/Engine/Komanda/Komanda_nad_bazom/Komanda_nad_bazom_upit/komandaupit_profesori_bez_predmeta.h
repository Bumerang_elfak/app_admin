#ifndef KOMANDAUPIT_PROFESORI_BEZ_PREDMETA_H
#define KOMANDAUPIT_PROFESORI_BEZ_PREDMETA_H

#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit.h"
#include "Engine/fje_za_koriscenje.h"

class komandaupit_profesori_bez_predmeta : public KomandaUpit
{
public:
    komandaupit_profesori_bez_predmeta();

    virtual QString toQuery(){
        return QString("SELECT * FROM ")+QString(TABELA_PROFESOR_BEZ_PREDMETA)
                +QString(";");
    }

    virtual QByteArray za_slanje(){
        //samo TIP
        using namespace Fje_za_Koriscenje;

        QString pom;
        pom.append(enkapsuliraj(QString::number(Tip)));

        QByteArray ba;
        ba.append(pom);
        return ba;
    }
    virtual int praviOd(QString &izvorniQString){

        using namespace Fje_za_Koriscenje;

        int iterator=0;
        QString pom;
        pom=dekapsulacija(izvorniQString,iterator);
        if( pom.toInt()!=KOMANDA_UPIT_PROFESOR_BEZ_PREDMETA )
            return 0;
        return 1;
    }

};

#endif // KOMANDAUPIT_PROFESORI_BEZ_PREDMETA_H
