#include "komandaupit_kviz.h"

komandaupit_kviz::komandaupit_kviz()
{
    Tabela=KVIZ;
    Tip=KOMANDA_UPIT_KVIZOVI;
}
komandaupit_kviz::~komandaupit_kviz()
{

}
QString komandaupit_kviz::toQuery()
{
    /*SELECT *
    FROM table
    WHERE [ conditions ]*/
    using namespace Fje_za_Koriscenje;

    QString values="";

    if(ImeKviza!="")
    {
        if(values!="")
            values=values + " AND "+ QString(TABELA_KVIZ_IME)+"=" +enkapsulirajZaBazu(ImeKviza);
        else
            values=values + QString(TABELA_KVIZ_IME)+"=" +enkapsulirajZaBazu(ImeKviza);
    }
    if(DinamickiKviz!=-1)
    {
        if(values!="")
            values=values + " AND "+ QString(TABELA_KVIZ_DINAMICKI)+"=" +enkapsulirajZaBazu(DinamickiKviz);
        else
            values=values + QString(TABELA_KVIZ_DINAMICKI)+"=" +enkapsulirajZaBazu(DinamickiKviz);
    }
    if(TezinaKviza!=-1)
    {
        if(values!="")
            values=values + " AND "+ QString(TABELA_KVIZ_TEZINA) +"="+enkapsulirajZaBazu(TezinaKviza);
        else
            values=values + QString(TABELA_KVIZ_TEZINA)+"=" +enkapsulirajZaBazu(TezinaKviza);
    }
    if(BrojPitanja!=-1)
    {
        if(values!="")
            values=values + " AND "+ QString(TABELA_KVIZ_BROJ_PITANJA)+"=" +enkapsulirajZaBazu(BrojPitanja);
        else
            values=values + QString(TABELA_KVIZ_BROJ_PITANJA)+"=" +enkapsulirajZaBazu(BrojPitanja);
    }
    if(IdProfesora!=-1)
    {
        if(values!="")
            values=values +" AND "+ QString(TABELA_KVIZ_ID_PROFESOR) +"="+enkapsulirajZaBazu(IdProfesora);
        else
            values=values + QString(TABELA_KVIZ_ID_PROFESOR) +"="+enkapsulirajZaBazu(IdProfesora);
    }
    if(IdPredmeta!=-1)
    {
        if(values!="")
            values=values + " AND "+ QString(TABELA_KVIZ_ID_PREDMET) +"="+enkapsulirajZaBazu(IdPredmeta);
        else
            values=values + QString(TABELA_KVIZ_ID_PREDMET) +"="+enkapsulirajZaBazu(IdPredmeta);
    }
    if(IdOblasti!=-1)
    {
        if(values!="")
            values=values +" AND "+ QString(TABELA_KVIZ_ID_OBLASTI) +"="+enkapsulirajZaBazu(IdOblasti);
        else
            values=values + QString(TABELA_KVIZ_ID_OBLASTI)+"=" +enkapsulirajZaBazu(IdOblasti);
    }
    if( IdKviza!=-1 ){
        if(values!="")
            values=values +" AND "+ QString(TABELA_KVIZ_ID) +"="+enkapsulirajZaBazu(IdKviza);
        else
            values=values + QString(TABELA_KVIZ_ID)+"=" +enkapsulirajZaBazu(IdKviza);
    }

    QString temp;
    if( values!="" )
        temp="SELECT * FROM " + QString(TABELA_KVIZ) +" WHERE "+values+";";
    else
        temp="SELECT * FROM " + QString(TABELA_KVIZ)+";";

    return temp;
}
QByteArray komandaupit_kviz::za_slanje()
{
    // Tip || Tabela || IdKviza || ImeKviza || DinamickiKviz || TezinaKviza || BrojPitanja
    // || IdProfesora || IdPredmeta || IdOblasti
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(QString::number(Tabela)));
    povratniString.append(enkapsuliraj(QString::number(IdKviza)));
    povratniString.append(enkapsuliraj(ImeKviza));
    povratniString.append(enkapsuliraj(QString::number(DinamickiKviz)));
    povratniString.append(enkapsuliraj(QString::number(TezinaKviza)));
    povratniString.append(enkapsuliraj(QString::number(BrojPitanja)));
    povratniString.append(enkapsuliraj(QString::number(IdProfesora)));
    povratniString.append(enkapsuliraj(QString::number(IdPredmeta)));
    povratniString.append(enkapsuliraj(QString::number(IdOblasti)));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}
int komandaupit_kviz::praviOd(QString &izvorniQString)
{
    // Tip || Tabela || IdKviza || ImeKviza || DinamickiKviz || TezinaKviza || BrojPitanja
    // || IdProfesora || IdPredmeta || IdOblasti
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KOMANDA_UPIT_KVIZOVI)
        return 0;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KVIZ)
        return 0;

    //dekapsulacija
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdKviza(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIme(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniDinamicki(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promenitezinu(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniBrojPitanja(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdProfesora(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdPredmeta(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdOblasti(pomocna.toInt());
    return iterator;
}
