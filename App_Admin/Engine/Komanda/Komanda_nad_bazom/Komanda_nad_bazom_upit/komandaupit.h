#ifndef KOMANDAUPIT_H
#define KOMANDAUPIT_H
#include <QString>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlDatabase>
#include <QObject>
#include "Engine/Komanda/KomandaNadBazom.h"
#include "Engine/Odgovor_na_komandu/error.h"
#include "Engine/tabele_i_kolone_iz_baze.h"
#include <iostream>

#include "Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_kvizovi.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_logovanje.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_oblasti.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_pitanja.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_predmeti.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_profesori.h"

class KomandaUpit : public KomandaNadBazom
{
    Q_OBJECT
public:
    KomandaUpit();
    virtual~KomandaUpit();

    virtual Odgovor_na_Komandu* izvrsiNadBazom(QSqlDatabase &baza);
    virtual QString toQuery()=0;

    virtual QByteArray za_slanje()=0;
    virtual int praviOd(QString &izvorniQString)=0;

    /*SELECT column1, column2
    FROM table1, table2
    WHERE [ conditions ]
    GROUP BY column1, column2
    HAVING [ conditions ]
    ORDER BY column1, column2*/
};

#endif // KOMANDAUPIT_H
