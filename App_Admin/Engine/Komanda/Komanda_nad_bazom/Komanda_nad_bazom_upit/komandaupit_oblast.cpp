#include "komandaupit_oblast.h"

komandaupit_oblast::komandaupit_oblast()
{
    Tabela=OBLAST;
    Tip=KOMANDA_UPIT_OBLASTI;
}

komandaupit_oblast::~komandaupit_oblast()
{

}

QString komandaupit_oblast::toQuery()
{
    /*SELECT *
    FROM table
    WHERE [ conditions ]*/
    using namespace Fje_za_Koriscenje;

    QString values="";

    if(ImeOblasti!=NULL && ImeOblasti!="")
    {
        if(values!="")
            values=values + " AND "+ QString(TABELA_OBLAST_IME)+"=" +enkapsulirajZaBazu(ImeOblasti);
        else
            values=values + QString(TABELA_OBLAST_IME) +"="+enkapsulirajZaBazu(ImeOblasti);
    }
    if(IdProfesora!=-1)
    {
        if(values!="")
            values=values + " AND "+ QString(TABELA_OBLAST_ID_PROFESORA)+"=" +enkapsulirajZaBazu(IdProfesora);
        else
            values=values + QString(TABELA_OBLAST_ID_PROFESORA) +"="+enkapsulirajZaBazu(IdProfesora);
    }
    if(IdPredmeta!=-1)
    {
        if(values!="")
            values=values +" AND "+ QString(TABELA_OBLAST_ID_PREDMETA)+"=" +enkapsulirajZaBazu(IdPredmeta);
        else
            values=values + QString(TABELA_OBLAST_ID_PREDMETA) +"="+enkapsulirajZaBazu(IdPredmeta);
    }
    if(IdOblasti!=-1){
        if(values!="")
            values=values +" AND "+ QString(TABELA_OBLAST_ID)+"=" +enkapsulirajZaBazu(IdOblasti);
        else
            values=values + QString(TABELA_OBLAST_ID) +"="+enkapsulirajZaBazu(IdOblasti);
    }

    QString temp;
    if( values!="" )
        temp="SELECT * FROM " + QString(TABELA_OBLAST) +" WHERE "+values+";";
    else
        temp="SELECT * FROM " + QString(TABELA_OBLAST) +";";

    return temp;
}
QByteArray komandaupit_oblast::za_slanje()
{
    // Tip || Tabela || IdOblasti || ImeOblasti || IdProfesora || IdPredmeta
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(QString::number(Tabela)));
    povratniString.append(enkapsuliraj(QString::number(IdOblasti)));
    povratniString.append(enkapsuliraj(ImeOblasti));
    povratniString.append(enkapsuliraj(QString::number(IdProfesora)));
    povratniString.append(enkapsuliraj(QString::number(IdPredmeta)));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}
int komandaupit_oblast::praviOd(QString &izvorniQString)
{
    // Tip || Tabela || IdOblasti || ImeOblasti || IdProfesora || IdPredmeta
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KOMANDA_UPIT_OBLASTI)
        return 0;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=OBLAST)
        return 0;

    //dekapsulacija
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdOblasti(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniImeOblasti(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdProfesora(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdPredmeta(pomocna.toInt());
    return iterator;
}
