#include "komandaupit_student.h"

komandaupit_student::komandaupit_student()
{
    Tabela=STUDENT;
    Tip=KOMANDA_UPIT_STUDENTI;
}

komandaupit_student::~komandaupit_student()
{

}
QString komandaupit_student::toQuery()
{
    /*SELECT *
    FROM table
    WHERE [ conditions ]*/
    using namespace Fje_za_Koriscenje;

    QString values="";

    if( IdStudenta!=-1 ){
        if(values!="")
            values=values +" AND "+ QString(TABELA_STUDENT_ID)+"=" +enkapsulirajZaBazu(IdStudenta);
        else
            values=values + QString(TABELA_STUDENT_ID)+"=" +enkapsulirajZaBazu(IdStudenta);

    }
    if(Username!="")
    {
        if(values!="")
            values=values +" AND "+ QString(TABELA_STUDENT_USERNAME)+"=" +enkapsulirajZaBazu(Username);
        else
            values=values + QString(TABELA_STUDENT_USERNAME)+"=" +enkapsulirajZaBazu(Username);
    }
    if(Password!="")
    {
        if(values!="")
            values=values + " AND "+ QString(TABELA_STUDENT_PASSWORD) +"="+enkapsulirajZaBazu(Password);
        else
            values=values + QString(TABELA_STUDENT_PASSWORD)+"=" +enkapsulirajZaBazu(Password);
    }

    QString temp;
    if( values!="" )
        temp="SELECT * FROM " + QString(TABELA_STUDENT) +" WHERE "+values+";";
    else
        temp="SELECT * FROM " + QString(TABELA_STUDENT) +";";

    return temp;
}
QByteArray komandaupit_student::za_slanje()
{
    // Tip || table || IdStudenta || Username || Password
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(QString::number(Tabela)));
    povratniString.append(enkapsuliraj(QString::number(IdStudenta)));
    povratniString.append(enkapsuliraj(Username));
    povratniString.append(enkapsuliraj(Password));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}
int komandaupit_student::praviOd(QString &izvorniQString)
{
    // Tip || table || IdStudenta || Username || Password
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KOMANDA_UPIT_STUDENTI)
        return 0;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=STUDENT)
        return 0;

    //dekapsulacija
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdStudenta(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniUsername(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniPassword(pomocna);

    return iterator;
}
