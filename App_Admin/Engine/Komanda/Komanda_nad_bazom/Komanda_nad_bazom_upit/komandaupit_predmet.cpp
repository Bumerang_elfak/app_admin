#include "komandaupit_predmet.h"

komandaupit_predmet::komandaupit_predmet()
{
    Tabela=PREDMET;
    Tip=KOMANDA_UPIT_PREDMETI;
}
komandaupit_predmet::~komandaupit_predmet()
{

}
QString komandaupit_predmet::toQuery()
{
    /*SELECT *
    FROM table
    WHERE [ conditions ]*/
    using namespace Fje_za_Koriscenje;

    QString values="";

    if(ImePredmeta!="")
    {
        if(values!="")
            values=values + " AND "+ QString(TABELA_PREDMET_IME)+"=" +enkapsulirajZaBazu(ImePredmeta);
        else
            values=values + QString(TABELA_PREDMET_IME) +"="+enkapsulirajZaBazu(ImePredmeta);
    }
    if(IdProfesora!=-1)
    {
        if(values!="")
            values=values + " AND "+ QString(TABELA_PREDMET_ID_PROFESORA) +"="+enkapsulirajZaBazu(IdProfesora);
        else
            values=values + QString(TABELA_PREDMET_ID_PROFESORA)+"=" +enkapsulirajZaBazu(IdProfesora);
    }
    if( IdPredmeta!=-1 ){
        if(values!="")
            values=values + " AND "+ QString(TABELA_PREDMET_ID) +"="+enkapsulirajZaBazu(IdPredmeta);
        else
            values=values + QString(TABELA_PREDMET_ID)+"=" +enkapsulirajZaBazu(IdPredmeta);
    }

    QString temp;
    if( values!="" )
        temp="SELECT * FROM " + QString(TABELA_PREDMET) +" WHERE "+values+";";
    else
        temp="SELECT * FROM " + QString(TABELA_PREDMET) +";";

    return temp;
}

QByteArray komandaupit_predmet::za_slanje()
{
    // Tip || Tabela || IdPredmeta || ImePredmeta || IdProfesora
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(QString::number(Tabela)));
        povratniString.append(enkapsuliraj(QString::number(IdPredmeta)));
    povratniString.append(enkapsuliraj(ImePredmeta));
    povratniString.append(enkapsuliraj(QString::number(IdProfesora)));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}
int komandaupit_predmet::praviOd(QString &izvorniQString)
{
    // Tip || Tabela || IdPredmeta || ImePredmeta || IdProfesora
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KOMANDA_UPIT_PREDMETI)
        return 0;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=PREDMET)
        return 0;

    //dekapsulacija

    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdPredmeta(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniImePredmeta(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdProfesora(pomocna.toInt());
    return iterator;
}
