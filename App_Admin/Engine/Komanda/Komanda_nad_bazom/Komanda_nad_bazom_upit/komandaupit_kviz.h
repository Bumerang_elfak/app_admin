#ifndef KOMANDAUPIT_KVIZ_H
#define KOMANDAUPIT_KVIZ_H

#include <QObject>
#include <QString>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlDatabase>

#include "Engine/Komanda/Komanda.h"
#include "Engine/Komanda/KomandaNadBazom.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_snimljen.h"
#include "Engine/Odgovor_na_komandu/error.h"
#include "Engine/tabele_i_kolone_iz_baze.h"
#include "Engine/fje_za_koriscenje.h"

class komandaupit_kviz : public KomandaUpit
{
    Q_OBJECT
public:
    komandaupit_kviz();
    ~komandaupit_kviz();

    void promeniIdKviza(int novo) {IdKviza=novo;}
    void promeniIme(QString novo) {ImeKviza=novo;}
    void promeniDinamicki(int novo){DinamickiKviz=(novo==0?0:1);}
    void promenitezinu(int novo){TezinaKviza=novo;}
    void promeniBrojPitanja(int novo){BrojPitanja=novo;}
    void promeniIdProfesora(int novo){IdProfesora=novo;}
    void promeniIdPredmeta(int novo){IdPredmeta=novo;}
    void promeniIdOblasti(int novo){IdOblasti=novo;}

    virtual QString toQuery();
    virtual QByteArray za_slanje();
    virtual int praviOd(QString &izvorniQString);
private:
    Tipovi Tabela;
    int IdKviza=-1;
    QString ImeKviza="";
    int DinamickiKviz=-1;
    int TezinaKviza=-1;
    int BrojPitanja=-1;
    int IdProfesora=-1;
    int IdPredmeta=-1;
    int IdOblasti=-1;
};

#endif // KOMANDAUPIT_KVIZ_H
