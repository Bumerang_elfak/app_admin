#ifndef KOMANDADODAJ_OBLAST_H
#define KOMANDADODAJ_OBLAST_H

#include <QObject>
#include <QString>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlDatabase>

#include "Engine/Komanda/Komanda.h"
#include "Engine/Komanda/KomandaNadBazom.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_snimljen.h"
#include "Engine/Odgovor_na_komandu/error.h"
#include "Engine/tabele_i_kolone_iz_baze.h"
#include "Engine/fje_za_koriscenje.h"

class komandadodaj_oblast : public KomandaDodaj
{
    Q_OBJECT
public:
    komandadodaj_oblast();
    ~komandadodaj_oblast();

    void promeniImeOblasti(QString novo) {ImeOblasti=novo;}
    void promeniIdProfesora(int novo){IdProfesora=novo;}
    void promeniIdPredmeta(int novo){IdPredmeta=novo;}

    virtual QString toQuery();
    virtual QByteArray za_slanje();
    virtual int praviOd(QString &izvorniQString);
private:
    QString ImeOblasti="";
    int IdProfesora;
    int IdPredmeta;

};

#endif // KOMANDADODAJ_OBLAST_H
