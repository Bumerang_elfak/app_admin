#include "komandadodaj_admin.h"

komandadodaj_admin::komandadodaj_admin()
{
    Tabela=ADMIN;
    adresa_povratka=NULL;
}

komandadodaj_admin::~komandadodaj_admin()
{

}

QString komandadodaj_admin::toQuery()
{
    /*INSERT INTO table_name
VALUES (value1,value2,value3,...);*/
    using namespace Fje_za_Koriscenje;
    QString temp="INSERT INTO " + QString(TABELA_ADMIN)+" ("+QString(TABELA_ADMIN_IME)+", "+QString(TABELA_ADMIN_PREZIME);
    temp=temp+", "+QString(TABELA_ADMIN_E_MAIL)+", "+QString(TABELA_ADMIN_USERNAME)+", "+QString(TABELA_ADMIN_PASSWORD)+") VALUES(";
    temp=temp+enkapsulirajZaBazu(Ime)+", "+enkapsulirajZaBazu(Prezime)+", "+enkapsulirajZaBazu(Email)+", "+enkapsulirajZaBazu(Username)+", "+enkapsulirajZaBazu(Password)+");";

    return temp;
}

QByteArray komandadodaj_admin::za_slanje()
{
    // Tip || table || Ime || Prezime || Email || Username || Password
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(QString::number(Tabela)));
    povratniString.append(enkapsuliraj(Ime));
    povratniString.append(enkapsuliraj(Prezime));
    povratniString.append(enkapsuliraj(Email));
    povratniString.append(enkapsuliraj(Username));
    povratniString.append(enkapsuliraj(Password));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}
int komandadodaj_admin::praviOd(QString &izvorniQString)
{
    // Tip || table || Ime || Prezime || Email || Username || Password
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KOMANDA_DODAJ)
        return 0;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=ADMIN)
        return 0;

    //dekapsulacija
    Tip=KOMANDA_DODAJ;
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIme(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniPrezime(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniEmail(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniUsername(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniPassword(pomocna);

    return iterator;
}
