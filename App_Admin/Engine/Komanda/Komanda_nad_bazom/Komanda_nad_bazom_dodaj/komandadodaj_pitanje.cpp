#include "komandadodaj_pitanje.h"

komandadodaj_pitanje::komandadodaj_pitanje()
{
    Tabela=PITANJE;
}
komandadodaj_pitanje::~komandadodaj_pitanje()
{

}

QString komandadodaj_pitanje::toQuery()
{
    /*INSERT INTO table_name
VALUES (value1,value2,value3,...);*/
    using namespace Fje_za_Koriscenje;
    QString temp="INSERT INTO " + QString(TABELA_PITANJA_ZA_KVIZ)+" ("+QString(TABELA_PITANJA_ZA_KVIZ_TEKST)+", "+QString(TABELA_PITANJA_ZA_KVIZ_ODG1);
    temp=temp+", "+QString(TABELA_PITANJA_ZA_KVIZ_ODG2)+", "+QString(TABELA_PITANJA_ZA_KVIZ_ODG3)+", "+QString(TABELA_PITANJA_ZA_KVIZ_ODG4)
            +", "+QString(TABELA_PITANJA_ZA_KVIZ_TACAN)+", "+QString(TABELA_PITANJA_ZA_KVIZ_TEZINA)+", "+QString(TABELA_KVIZ_ID_PROFESOR)
            +", "+QString(TABELA_KVIZ_ID_PREDMET)+", "+QString(TABELA_KVIZ_ID_OBLASTI)+") VALUES(";
    temp=temp+enkapsulirajZaBazu(TekstPitanja)+", "+enkapsulirajZaBazu(Odgovor1)+", "+enkapsulirajZaBazu(Odgovor2)+", "
            +enkapsulirajZaBazu(Odgovor3)+", "+enkapsulirajZaBazu(Odgovor4)+", "+enkapsulirajZaBazu(TacanOdgovor)+", "+enkapsulirajZaBazu(TezinaPitanja)+", "
            +enkapsulirajZaBazu(IdProfesora)+", "+enkapsulirajZaBazu(IdPredmeta)+", "+enkapsulirajZaBazu(IdOblasti)+");";

    return temp;
}

QByteArray komandadodaj_pitanje::za_slanje()
{
    // Tip || Tabela || TekstPitanja || Odgovor1 || Odgovor2 || Odgovor3 || Odgovor4
    // TacanOdgovor || TezinaPitanja || IdProfesora || IdPredmeta || IdOblasti
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(QString::number(Tabela)));
    povratniString.append(enkapsuliraj(TekstPitanja));
    povratniString.append(enkapsuliraj(Odgovor1));
    povratniString.append(enkapsuliraj(Odgovor2));
    povratniString.append(enkapsuliraj(Odgovor3));
    povratniString.append(enkapsuliraj(Odgovor4));
    povratniString.append(enkapsuliraj(QString::number(TacanOdgovor)));
    povratniString.append(enkapsuliraj(QString::number(TezinaPitanja)));
    povratniString.append(enkapsuliraj(QString::number(IdProfesora)));
    povratniString.append(enkapsuliraj(QString::number(IdPredmeta)));
    povratniString.append(enkapsuliraj(QString::number(IdOblasti)));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}
int komandadodaj_pitanje::praviOd(QString &izvorniQString)
{
    // Tip || Tabela || TekstPitanja || Odgovor1 || Odgovor2 || Odgovor3 || Odgovor4
    // TacanOdgovor || TezinaPitanja || IdProfesora || IdPredmeta || IdOblasti
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KOMANDA_DODAJ)
        return 0;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=PITANJE)
        return 0;

    //dekapsulacija
    Tip=KOMANDA_DODAJ;
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniTekstPitanja(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniOdgovor1(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniOdgovor2(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniOdgovor3(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniOdgovor4(pomocna);

    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniTacanOdgovor(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniTezinuPitanja(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdProfesora(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdPredmeta(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdOblasti(pomocna.toInt());
    return iterator;
}
