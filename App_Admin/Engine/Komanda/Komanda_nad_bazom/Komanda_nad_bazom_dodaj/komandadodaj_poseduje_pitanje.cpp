#include "komandadodaj_poseduje_pitanje.h"

komandadodaj_poseduje_pitanje::komandadodaj_poseduje_pitanje()
{
    Tabela=POSEDUJE_PITANJE;
}
komandadodaj_poseduje_pitanje::~komandadodaj_poseduje_pitanje()
{

}

QString komandadodaj_poseduje_pitanje::toQuery()
{
    /*INSERT INTO table_name
VALUES (value1,value2,value3,...);*/
    using namespace Fje_za_Koriscenje;
    QString temp="INSERT INTO " + QString(TABELA_POSEDUJE_PITANJE)+" ("
            +QString(TABELA_POSEDUJE_PITANJE_ID_KVIZA);
    temp=temp+", "+QString(TABELA_POSEDUJE_PITANJE_ID_PITANJA)+") VALUES(";
    temp=temp+enkapsulirajZaBazu(IdKviza)+", "+enkapsulirajZaBazu(IdPitanja)+");";

    return temp;
}
QByteArray komandadodaj_poseduje_pitanje::za_slanje()
{
    // Tip || Tabela || IdPitanja || IdKviza
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(QString::number(Tabela)));
    povratniString.append(enkapsuliraj(QString::number(IdPitanja)));
    povratniString.append(enkapsuliraj(QString::number(IdKviza)));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}
int komandadodaj_poseduje_pitanje::praviOd(QString &izvorniQString)
{
    // Tip || Tabela || IdPitanja || IdKviza
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KOMANDA_DODAJ)
        return 0;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=POSEDUJE_PITANJE)
        return 0;

    //dekapsulacija
    Tip=KOMANDA_DODAJ;
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdPitanja(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdKviza(pomocna.toInt());
    return iterator;
}
