#include "komandadodaj_oblast.h"

komandadodaj_oblast::komandadodaj_oblast()
{
    Tabela=OBLAST;
}

komandadodaj_oblast::~komandadodaj_oblast()
{

}

QString komandadodaj_oblast::toQuery()
{
    /*INSERT INTO table_name
VALUES (value1,value2,value3,...);*/
    using namespace Fje_za_Koriscenje;
    QString temp="INSERT INTO " + QString(TABELA_OBLAST)+" ("+QString(TABELA_OBLAST_IME)+", "+QString(TABELA_OBLAST_ID_PROFESORA);
    temp=temp+", "+QString(TABELA_OBLAST_ID_PREDMETA)+") VALUES(";
    temp=temp+enkapsulirajZaBazu(ImeOblasti)+", "+enkapsulirajZaBazu(IdProfesora)+", "+enkapsulirajZaBazu(IdPredmeta)+");";

    return temp;
}
QByteArray komandadodaj_oblast::za_slanje()
{
    // Tip || Tabela || ImeOblasti || IdProfesora || IdPredmeta
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(QString::number(Tabela)));
    povratniString.append(enkapsuliraj(ImeOblasti));
    povratniString.append(enkapsuliraj(QString::number(IdProfesora)));
    povratniString.append(enkapsuliraj(QString::number(IdPredmeta)));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}
int komandadodaj_oblast::praviOd(QString &izvorniQString)
{
    // Tip || Tabela || ImeOblasti || IdProfesora || IdPredmeta
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KOMANDA_DODAJ)
        return 0;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=OBLAST)
        return 0;

    //dekapsulacija
    Tip=KOMANDA_DODAJ;
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniImeOblasti(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdProfesora(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdPredmeta(pomocna.toInt());
    return iterator;
}
