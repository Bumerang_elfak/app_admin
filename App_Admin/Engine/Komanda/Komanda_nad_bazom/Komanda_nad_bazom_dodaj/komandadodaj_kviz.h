#ifndef KOMANDADODAJ_KVIZ_H
#define KOMANDADODAJ_KVIZ_H

#include <QObject>
#include <QString>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlDatabase>

#include "Engine/Komanda/Komanda.h"
#include "Engine/Komanda/KomandaNadBazom.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_snimljen.h"
#include "Engine/Odgovor_na_komandu/error.h"
#include "Engine/tabele_i_kolone_iz_baze.h"
#include "Engine/fje_za_koriscenje.h"

class komandadodaj_kviz : public KomandaDodaj
{
    Q_OBJECT
public:
    komandadodaj_kviz();
    ~komandadodaj_kviz();

    void promeniIme(QString novo) {ImeKviza=novo;}
    void promeniDinamicki(bool novo){DinamickiKviz=novo;}
    void promenitezinu(int novo){TezinaKviza=novo;}
    void promeniBrojPitanja(int novo){BrojPitanja=novo;}
    void promeniIdProfesora(int novo){IdProfesora=novo;}
    void promeniIdPredmeta(int novo){IdPredmeta=novo;}
    void promeniIdOblasti(int novo){IdOblasti=novo;}

    virtual QString toQuery();
    virtual QByteArray za_slanje();
    virtual int praviOd(QString &izvorniQString);
private:
    QString ImeKviza="";
    bool DinamickiKviz;
    int TezinaKviza;
    int BrojPitanja;
    int IdProfesora;
    int IdPredmeta;
    int IdOblasti;
};

#endif // KOMANDADODAJ_KVIZ_H
