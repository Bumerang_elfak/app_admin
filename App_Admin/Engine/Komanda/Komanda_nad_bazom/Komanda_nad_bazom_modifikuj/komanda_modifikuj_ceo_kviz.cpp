#include "komanda_modifikuj_ceo_kviz.h"

Komanda_Modifikuj_Ceo_Kviz::Komanda_Modifikuj_Ceo_Kviz(){
    Tip=KOMANDA_MODIFIKUJ_CEO_KVIZ;
}
Odgovor_na_Komandu* Komanda_Modifikuj_Ceo_Kviz::izvrsiNadBazom(QSqlDatabase &baza){
    //samo da zamene mesta obavezno
    kviz_za_modifikaciju.izracunaj_tezinu();
    Odgovor_na_Komandu *odg=NULL;
    baza.transaction();
    QList<Pitanje> pitanja_sva=kviz_za_modifikaciju.vrati_pitanja();
    for( int i=0; i<pitanja_sva.count(); i++ ){
        Komanda_Modifikuj_celo_Pitanje kom_za_pitanje;
        kom_za_pitanje.postavi_pitanje(pitanja_sva[i]);
        odg=kom_za_pitanje.izvrsiNadBazom(baza);
        if( odg==NULL || odg->vrati_tip()==ERROR ){
            baza.rollback();
            return odg;
        }
        delete odg;
    }

    komandamodifikuj_kviz kom_kviz;
    kom_kviz.promeniIdKviza(kviz_za_modifikaciju.vrati_ID());
    kom_kviz.promenitezinu(kviz_za_modifikaciju.vrati_tezinu());

    odg=kom_kviz.izvrsiNadBazom(baza);
    if( odg==NULL || odg->vrati_tip()==ERROR ){
        baza.rollback();
        return odg;
    }
    delete odg;

    Odgovor_Modifikovan *odgovor_kraj=new Odgovor_Modifikovan();
    odgovor_kraj->postaviOK(true);
    return odgovor_kraj;
}
QByteArray Komanda_Modifikuj_Ceo_Kviz::za_slanje(){
    // TIP || KVIZ
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(kviz_za_modifikaciju.za_slanje()));

    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;

}
int Komanda_Modifikuj_Ceo_Kviz::praviOd(QString &izvorniQString){
    // TIP || KVIZ
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KOMANDA_MODIFIKUJ_CEO_KVIZ)
        return 0;

    QString ba;
    ba.append(dekapsulacija(pomocna,iterator));
    kviz_za_modifikaciju.pravi_od(ba);
    return iterator;
}
