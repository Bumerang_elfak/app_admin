#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj.h"

KomandaModifikuj::KomandaModifikuj()
{
    Tip=KOMANDA_MODIFIKUJ;
}

KomandaModifikuj::~KomandaModifikuj()
{

}

Odgovor_na_Komandu *KomandaModifikuj::izvrsiNadBazom(QSqlDatabase &baza)
{
    //vidi oko uslova
    QSqlQuery query(baza);
    QString upit=toQuery();
    Odgovor_na_Komandu* k=NULL;

    if(upit==NULL || upit=="")
        return 0;

    std::cout<<upit.toStdString()<<std::endl;
    if(upit!="" && query.exec(upit)){
        //lepo izvrseno
        k=new Odgovor_Modifikovan();
        k->postavi_adresu_povratka(adresa_povratka);
    }
    else{
        //greska prilikom upita
        k=new Error();
        k->postavi_adresu_povratka(adresa_povratka);
        qobject_cast<Error*>(k)->postavi_greska("Greska prilikom modifikovanja podatka u bazi");
    }
    return k;
}
