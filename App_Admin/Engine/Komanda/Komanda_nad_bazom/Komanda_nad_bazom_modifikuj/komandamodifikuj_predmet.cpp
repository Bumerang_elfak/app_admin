#include "komandamodifikuj_predmet.h"

komandamodifikuj_predmet::komandamodifikuj_predmet()
{
    Tabela=PREDMET;
}
komandamodifikuj_predmet::~komandamodifikuj_predmet()
{

}
QString komandamodifikuj_predmet::toQuery()
{
    /*UPDATE table_name
    SET column1=value1,column2=value2,...
    WHERE TABLE_ID=ID;*/
    using namespace Fje_za_Koriscenje;

    QString values="";

    if(ImePredmeta!=NULL && ImePredmeta!="")
    {
        if(values!="")
            values=values + ", "+ QString(TABELA_PREDMET_IME)+"=" +enkapsulirajZaBazu(ImePredmeta);
        else
            values=values + QString(TABELA_PREDMET_IME)+"=" +enkapsulirajZaBazu(ImePredmeta);
    }
    if(IdProfesora!=-1)
    {
        if(values!="")
            values=values + ", "+ QString(TABELA_PREDMET_ID_PROFESORA) +"="+enkapsulirajZaBazu(IdProfesora);
        else
            values=values + QString(TABELA_PREDMET_ID_PROFESORA)+"=" +enkapsulirajZaBazu(IdProfesora);
    }

    QString temp="UPDATE " + QString(TABELA_PREDMET) +" SET "+values+ " WHERE "+
            QString(TABELA_PREDMET_ID)+"="+QString::number(IdPredmeta)+";";

    return temp;
}

QByteArray komandamodifikuj_predmet::za_slanje()
{
    // Tip || Tabela || IdPredmeta || ImePredmeta || IdProfesora
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(QString::number(Tabela)));
        povratniString.append(enkapsuliraj(QString::number(IdPredmeta)));
    povratniString.append(enkapsuliraj(ImePredmeta));
    povratniString.append(enkapsuliraj(QString::number(IdProfesora)));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}
int komandamodifikuj_predmet::praviOd(QString &izvorniQString)
{
    // Tip || Tabela || IdPredmeta || ImePredmeta || IdProfesora
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KOMANDA_MODIFIKUJ)
        return 0;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=PREDMET)
        return 0;

    //dekapsulacija

    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdPredmeta(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniImePredmeta(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdProfesora(pomocna.toInt());
    return iterator;
}
