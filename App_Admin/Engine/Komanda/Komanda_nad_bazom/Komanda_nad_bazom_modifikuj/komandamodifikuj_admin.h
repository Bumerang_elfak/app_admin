#ifndef KOMANDAMODIFIKUJ_ADMIN_H
#define KOMANDAMODIFIKUJ_ADMIN_H

#include <QObject>
#include <QString>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlDatabase>

#include "Engine/Komanda/Komanda.h"
#include "Engine/Komanda/KomandaNadBazom.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_snimljen.h"
#include "Engine/Odgovor_na_komandu/error.h"
#include "Engine/tabele_i_kolone_iz_baze.h"
#include "Engine/fje_za_koriscenje.h"


class komandamodifikuj_admin : public KomandaModifikuj
{
    Q_OBJECT
public:
    komandamodifikuj_admin();
    ~komandamodifikuj_admin();

    void promeniIdAdmina(int novo) {IdAdmina=novo;}
    void promeniIme(QString novo) {Ime=novo;}
    void promeniPrezime(QString novo){Prezime=novo;}
    void promeniEmail(QString novo){Email=novo;}
    void promeniUsername(QString novo){Username=novo;}
    void promeniPassword(QString novo){Password=novo;}

    virtual QString toQuery();
    virtual QByteArray za_slanje();
    virtual int praviOd(QString &izvorniQString);
private:
    Tipovi Tabela;
    int IdAdmina;
    QString Ime="";
    QString Prezime="";
    QString Email="";
    QString Username="";
    QString Password="";

};

#endif // KOMANDAMODIFIKUJ_ADMIN_H
