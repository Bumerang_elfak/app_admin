#ifndef KOMANDA_VRATI_SVE_LOGOVANE_PROFESORE_H
#define KOMANDA_VRATI_SVE_LOGOVANE_PROFESORE_H

#include "Engine/Komanda/Komanda.h"
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"

#include "Engine/tabele_i_kolone_iz_baze.h"
#include "Engine/Odgovor_na_komandu/error.h"

class komanda_vrati_sve_logovane_profesore : public Komanda
{
    Q_OBJECT
public:
    komanda_vrati_sve_logovane_profesore();
    ~komanda_vrati_sve_logovane_profesore();

    virtual QByteArray za_slanje();
    virtual int praviOd(QString &izvorniBitArray);
};

#endif // KOMANDA_VRATI_SVE_LOGOVANE_PROFESORE_H
