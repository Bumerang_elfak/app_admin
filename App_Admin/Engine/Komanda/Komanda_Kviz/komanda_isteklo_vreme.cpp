#include "Engine/Komanda/Komanda_Kviz/komanda_isteklo_vreme.h"

Komanda_isteklo_vreme::Komanda_isteklo_vreme(){
    Tip=KOMANDA_KVIZ_ISTEKLO_VREME;
}
Komanda_isteklo_vreme::~Komanda_isteklo_vreme(){}
QByteArray  Komanda_isteklo_vreme::za_slanje(){
    //TIP || ID_KVIZ
    using namespace Fje_za_Koriscenje;

    QString s;
    s.append(enkapsuliraj(QString::number(Tip)));
    s.append(enkapsuliraj(QString::number(ID_Aktivan_kviz)));
    QByteArray ba;
    ba.append(s);
    return ba;
}
int        Komanda_isteklo_vreme::praviOd(QString &s){
    //TIP
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pom;

    pom=dekapsulacija(s,iterator);
    if( pom.toInt()!=KOMANDA_KVIZ_ISTEKLO_VREME )
        return 0;

    pom=dekapsulacija(s,iterator);
    ID_Aktivan_kviz=pom.toInt();
    return iterator;
}
