#include "Engine/Komanda/Komanda_Kviz/komanda_kviz_prikazi_statistiku.h"

Komanda_Kviz_Prikazi_Statistiku::Komanda_Kviz_Prikazi_Statistiku()
{
    Tip=KOMANDA_KVIZ_PRIKAZI_STATISTIKU;
}
Komanda_Kviz_Prikazi_Statistiku::~Komanda_Kviz_Prikazi_Statistiku(){}

QByteArray  Komanda_Kviz_Prikazi_Statistiku::za_slanje(){

    //TIP || ID_AKT_KVIZ
    using namespace Fje_za_Koriscenje;

    QString s;
    s.append(enkapsuliraj(QString::number(Tip)));
    s.append(enkapsuliraj(QString::number(ID_Aktivan_kviz)));
    QByteArray ba;
    ba.append(s);
    return ba;

}
int        Komanda_Kviz_Prikazi_Statistiku::praviOd(QString &s){

    //TIP || ID_AKT_KVIZ
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pom;

    pom=dekapsulacija(s,iterator);
    if( pom.toInt()!=KOMANDA_KVIZ_PRIKAZI_STATISTIKU )
        return 0;

    pom=dekapsulacija(s,iterator);
    ID_Aktivan_kviz=pom.toInt();
    return iterator;

}
