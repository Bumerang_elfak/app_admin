#ifndef KOMANDA_KVIZ_NORMALAN_PREKID_H
#define KOMANDA_KVIZ_NORMALAN_PREKID_H

#include "Engine/Komanda/komanda_kviz.h"

class Komanda_Kviz_Normalan_Prekid : public Komanda_Kviz
{
    Q_OBJECT
public:
    Komanda_Kviz_Normalan_Prekid();
    ~Komanda_Kviz_Normalan_Prekid();

    virtual QByteArray  za_slanje();
    virtual int        praviOd(QString &s);
};

#endif // KOMANDA_KVIZ_NORMALAN_PREKID_H
