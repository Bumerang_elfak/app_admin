#ifndef KOMANDA_ISTEKLO_VREME_H
#define KOMANDA_ISTEKLO_VREME_H

#include "Engine/Komanda/komanda_kviz.h"

class Komanda_isteklo_vreme : public Komanda_Kviz
{
    Q_OBJECT
public:
    Komanda_isteklo_vreme();

    ~Komanda_isteklo_vreme();
    virtual QByteArray  za_slanje();
    virtual int         praviOd(QString &s);
};

#endif // KOMANDA_ISTEKLO_VREME_H
