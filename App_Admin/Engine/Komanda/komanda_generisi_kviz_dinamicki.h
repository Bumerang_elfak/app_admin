#ifndef KOMANDA_GENERISI_KVIZ_DINAMICKI_H
#define KOMANDA_GENERISI_KVIZ_DINAMICKI_H

#include "Engine/Komanda/Komanda.h"
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"
#include "Engine/Komanda/KomandaNadBazom.h"

#include "Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_generisi_kviz_dinamicki.h"
#include "Engine/tabele_i_kolone_iz_baze.h"
#include "Engine/Odgovor_na_komandu/error.h"

class Komanda_Generisi_Kviz_Dinamicki
        : public KomandaNadBazom
{
    Q_OBJECT
private:
    int tezina=100;
    int ID_OBLAST=-1;
    int br_pitanja=-1;
    QString upit_koji_se_treba_izvrsiti=""; //ili toQuery ili ovo, ne treba oba

public:
    Komanda_Generisi_Kviz_Dinamicki();
    ~Komanda_Generisi_Kviz_Dinamicki();

    void postavi_tezina(int t){
        tezina=t;
    }
    int vrati_tezina(){
        return tezina;
    }

    void postavi_ID_OBLAST(int id){
        ID_OBLAST=id;
    }
    int vrati_ID_OBLAST(){
        return ID_OBLAST;
    }

    void postavi_br_pitanja( int br ){
        br_pitanja=br;
    }
    int vrati_br_pitanaj(){
        return br_pitanja;
    }
    virtual QByteArray  za_slanje();
    virtual int         praviOd(QString &izvorniBitArray);
    Odgovor_na_Komandu* izvrsiNadBazom(QSqlDatabase &baza); //zameni posle za istancu baze, stavi sta treba
    QString toQuery();
};

#endif // KOMANDA_GENERISI_KVIZ_DINAMICKI_H
