#ifndef KOMUNIKACIJA_SA_SERVEROM_H
#define KOMUNIKACIJA_SA_SERVEROM_H

//OVDE FJA RUN!!! ZATO STO JOJ FALI DA PRAVI OBJEKAT

#include <QThread>
#include <QTcpSocket>
#include <QHostAddress>
#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include "Engine/Komanda/Komanda.h"
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"
#include "Engine/fje_za_koriscenje.h"
#include "Engine/fje_za_kreiranje.h"
#include "Engine/komunikacija_sa_serverom_worker.h"



class Komunikacija_sa_Serverom : public QObject
{
    Q_OBJECT
private:

    static Komunikacija_sa_Serverom *obj;

    QThread Worker;


    Komunikacija_sa_Serverom();
    ~Komunikacija_sa_Serverom();

public:

    static Komunikacija_sa_Serverom* vrati_komunikaciju(){
        if (obj==NULL)
            obj=new Komunikacija_sa_Serverom();
        return obj;
    }
    static bool unisti(){
        delete obj;
        obj=NULL;
        return true;
    }

signals:
    void stigo_odg(Odgovor_na_Komandu* odgovor);
    void salji_workeru( Komanda *k );
    void pravi_socket();

public slots:
    void salji_komandu( Komanda* k );
    void prihvati_od_workera(Odgovor_na_Komandu* odg);
    void slot_worker_run();
    void slot_worker_end();
};

#endif // KOMUNIKACIJA_SA_SERVEROM_H
