#-------------------------------------------------
#
# Project created by QtCreator 2016-04-19T16:45:45
#
#-------------------------------------------------

QT       += core gui
QT += core network sql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
QT+=network
CONFIG += c++11
TARGET = Forma
TEMPLATE = app




SOURCES += main.cpp\
    adminwindow.cpp \
    dodajpredmet.cpp \
    dodajprofesora.cpp \
    loginwindow.cpp \
    View/view.cpp \
    View/view2.cpp \
    Controller/controller.cpp \
    Engine/Komanda/Komanda_Kviz/komanda_isteklo_vreme.cpp \
    Engine/Komanda/Komanda_Kviz/komanda_kviz_aktiviraj.cpp \
    Engine/Komanda/Komanda_Kviz/komanda_kviz_log.cpp \
    Engine/Komanda/Komanda_Kviz/komanda_kviz_nasilno_prekini.cpp \
    Engine/Komanda/Komanda_Kviz/komanda_kviz_normalan_prekid.cpp \
    Engine/Komanda/Komanda_Kviz/komanda_kviz_odg_pitanje.cpp \
    Engine/Komanda/Komanda_Kviz/komanda_kviz_prikazi_statistiku.cpp \
    Engine/Komanda/Komanda_Kviz/komanda_kviz_sl_pitanje.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_admin.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_ceo_kviz.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_kviz.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_oblast.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_pitanje.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_poseduje_pitanje.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_predmet.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_profesor.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_student.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_admin.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_kviz.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_oblast.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_pitanje.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_predmet.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_profesor.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_student.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_admin.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_kviz.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_oblast.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_pitanja_za_kviz.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_pitanje.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_predmet.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_profesor.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_profesori_bez_predmeta.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_student.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_sve_za_profesora.cpp \
    Engine/Komanda/Komanda_nad_bazom/komandabrisi.cpp \
    Engine/Komanda/Komanda.cpp \
    Engine/Komanda/komanda_generisi_kviz_dinamicki.cpp \
    Engine/Komanda/komanda_kviz.cpp \
    Engine/Komanda/komanda_logovanje.cpp \
    Engine/Komanda/komanda_vrati_sve_aktivne_kvizove.cpp \
    Engine/Komanda/komanda_vrati_sve_logovane_profesore.cpp \
    Engine/Komanda/KomandaNadBazom.cpp \
    Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_aktivan_kviz.cpp \
    Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_generisi_kviz_dinamicki.cpp \
    Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_isteklo_vreme.cpp \
    Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_nasilan_prekid.cpp \
    Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_sledece_pitanje.cpp \
    Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_svi_odgovorili.cpp \
    Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_tren_statistika_pitanja.cpp \
    Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_uspesna_prijava.cpp \
    Engine/Odgovor_na_komandu/Odgovori_Kviz/Odgovor_Uspesno_zavrsen_Kviz.cpp \
    Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_modifikovan.cpp \
    Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_obrisan.cpp \
    Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_snimljen.cpp \
    Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_kvizovi.cpp \
    Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_logovanje.cpp \
    Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_oblasti.cpp \
    Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_pitanja.cpp \
    Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_predmeti.cpp \
    Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_profesori.cpp \
    Engine/Odgovor_na_komandu/error.cpp \
    Engine/Odgovor_na_komandu/error_diskonektovan_sa_servera.cpp \
    Engine/Odgovor_na_komandu/odgovor_na_komandu.cpp \
    Engine/Osobe/admin.cpp \
    Engine/Osobe/osobe.cpp \
    Engine/Osobe/profesor.cpp \
    Engine/Osobe/student.cpp \
    Engine/PPOK/kviz.cpp \
    Engine/PPOK/oblast.cpp \
    Engine/PPOK/pitanje.cpp \
    Engine/PPOK/ppok.cpp \
    Engine/PPOK/predmet.cpp \
    Engine/fje_za_koriscenje.cpp \
    Engine/fje_za_kreiranje.cpp \
    Engine/komunikacija_sa_serverom.cpp \
    Engine/komunikacija_sa_serverom_worker.cpp \
    Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_cekaj_sledece_pitanje.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_kvizovi_za_pitanja.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komanda_modifikuj_celo_pitanje.cpp \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komanda_modifikuj_ceo_kviz.cpp



HEADERS  += \
    adminwindow.h \
    dodajpredmet.h \
    dodajprofesora.h \
    loginwindow.h \
    View/view.h \
    View/view2.h \
    Controller/controller.h \
    Engine/Komanda/Komanda_Kviz/komanda_isteklo_vreme.h \
    Engine/Komanda/Komanda_Kviz/komanda_kviz_aktiviraj.h \
    Engine/Komanda/Komanda_Kviz/komanda_kviz_log.h \
    Engine/Komanda/Komanda_Kviz/komanda_kviz_nasilno_prekini.h \
    Engine/Komanda/Komanda_Kviz/komanda_kviz_normalan_prekid.h \
    Engine/Komanda/Komanda_Kviz/komanda_kviz_odg_pitanje.h \
    Engine/Komanda/Komanda_Kviz/komanda_kviz_prikazi_statistiku.h \
    Engine/Komanda/Komanda_Kviz/komanda_kviz_sl_pitanje.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_admin.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_ceo_kviz.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_kviz.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_oblast.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_pitanje.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_poseduje_pitanje.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_predmet.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_profesor.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_student.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_admin.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_kviz.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_oblast.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_pitanje.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_predmet.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_profesor.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_student.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_admin.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_kviz.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_oblast.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_pitanja_za_kviz.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_pitanje.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_predmet.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_profesor.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_profesori_bez_predmeta.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_student.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_sve_za_profesora.h \
    Engine/Komanda/Komanda_nad_bazom/komandabrisi.h \
    Engine/Komanda/Komanda.h \
    Engine/Komanda/komanda_generisi_kviz_dinamicki.h \
    Engine/Komanda/komanda_kviz.h \
    Engine/Komanda/komanda_logovanje.h \
    Engine/Komanda/komanda_vrati_sve_aktivne_kvizove.h \
    Engine/Komanda/komanda_vrati_sve_logovane_profesore.h \
    Engine/Komanda/KomandaNadBazom.h \
    Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_aktivan_kviz.h \
    Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_generisi_kviz_dinamicki.h \
    Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_isteklo_vreme.h \
    Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_nasilan_prekid.h \
    Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_sledece_pitanje.h \
    Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_svi_odgovorili.h \
    Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_tren_statistika_pitanja.h \
    Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_uspesna_prijava.h \
    Engine/Odgovor_na_komandu/Odgovori_Kviz/Odgovor_Uspesno_zavrsen_Kviz.h \
    Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_modifikovan.h \
    Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_obrisan.h \
    Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_snimljen.h \
    Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_kvizovi.h \
    Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_logovanje.h \
    Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_oblasti.h \
    Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_pitanja.h \
    Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_predmeti.h \
    Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_profesori.h \
    Engine/Odgovor_na_komandu/error.h \
    Engine/Odgovor_na_komandu/error_diskonektovan_sa_servera.h \
    Engine/Odgovor_na_komandu/odgovor_na_komandu.h \
    Engine/Osobe/admin.h \
    Engine/Osobe/osobe.h \
    Engine/Osobe/profesor.h \
    Engine/Osobe/student.h \
    Engine/PPOK/kviz.h \
    Engine/PPOK/oblast.h \
    Engine/PPOK/pitanje.h \
    Engine/PPOK/ppok.h \
    Engine/PPOK/predmet.h \
    Engine/Enums.h \
    Engine/fje_za_koriscenje.h \
    Engine/fje_za_kreiranje.h \
    Engine/komunikacija_sa_serverom.h \
    Engine/komunikacija_sa_serverom_worker.h \
    Engine/tabele_i_kolone_iz_baze.h \
    Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_cekaj_sledece_pitanje.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_kvizovi_za_pitanja.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komanda_modifikuj_celo_pitanje.h \
    Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komanda_modifikuj_ceo_kviz.h



FORMS    += \
    adminwindow.ui \
    dodajpredmet.ui \
    dodajprofesora.ui \
    loginwindow.ui

RESOURCES += \
    izgled.qrc
