/********************************************************************************
** Form generated from reading UI file 'dodajprofesora.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DODAJPROFESORA_H
#define UI_DODAJPROFESORA_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_DodajProfesora
{
public:
    QVBoxLayout *verticalLayout_3;
    QLabel *label;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QVBoxLayout *verticalLayout;
    QLineEdit *imeEdit;
    QLineEdit *prezimeEdit;
    QLineEdit *emailEdit;
    QLineEdit *usernameEdit;
    QLineEdit *passwordEdit;
    QSpacerItem *verticalSpacer_2;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *DodajProfesora)
    {
        if (DodajProfesora->objectName().isEmpty())
            DodajProfesora->setObjectName(QStringLiteral("DodajProfesora"));
        DodajProfesora->resize(424, 296);
        verticalLayout_3 = new QVBoxLayout(DodajProfesora);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        label = new QLabel(DodajProfesora);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout_3->addWidget(label);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label_2 = new QLabel(DodajProfesora);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_2->addWidget(label_2);

        label_3 = new QLabel(DodajProfesora);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout_2->addWidget(label_3);

        label_4 = new QLabel(DodajProfesora);
        label_4->setObjectName(QStringLiteral("label_4"));

        verticalLayout_2->addWidget(label_4);

        label_5 = new QLabel(DodajProfesora);
        label_5->setObjectName(QStringLiteral("label_5"));

        verticalLayout_2->addWidget(label_5);

        label_6 = new QLabel(DodajProfesora);
        label_6->setObjectName(QStringLiteral("label_6"));

        verticalLayout_2->addWidget(label_6);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        imeEdit = new QLineEdit(DodajProfesora);
        imeEdit->setObjectName(QStringLiteral("imeEdit"));
        imeEdit->setMaxLength(15);

        verticalLayout->addWidget(imeEdit);

        prezimeEdit = new QLineEdit(DodajProfesora);
        prezimeEdit->setObjectName(QStringLiteral("prezimeEdit"));
        prezimeEdit->setMaxLength(15);

        verticalLayout->addWidget(prezimeEdit);

        emailEdit = new QLineEdit(DodajProfesora);
        emailEdit->setObjectName(QStringLiteral("emailEdit"));
        emailEdit->setMaxLength(30);

        verticalLayout->addWidget(emailEdit);

        usernameEdit = new QLineEdit(DodajProfesora);
        usernameEdit->setObjectName(QStringLiteral("usernameEdit"));
        usernameEdit->setMaxLength(30);

        verticalLayout->addWidget(usernameEdit);

        passwordEdit = new QLineEdit(DodajProfesora);
        passwordEdit->setObjectName(QStringLiteral("passwordEdit"));
        passwordEdit->setMaxLength(30);

        verticalLayout->addWidget(passwordEdit);


        horizontalLayout->addLayout(verticalLayout);


        verticalLayout_3->addLayout(horizontalLayout);

        verticalSpacer_2 = new QSpacerItem(20, 10, QSizePolicy::Minimum, QSizePolicy::Minimum);

        verticalLayout_3->addItem(verticalSpacer_2);

        buttonBox = new QDialogButtonBox(DodajProfesora);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout_3->addWidget(buttonBox);


        retranslateUi(DodajProfesora);
        QObject::connect(buttonBox, SIGNAL(accepted()), DodajProfesora, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), DodajProfesora, SLOT(reject()));

        QMetaObject::connectSlotsByName(DodajProfesora);
    } // setupUi

    void retranslateUi(QDialog *DodajProfesora)
    {
        DodajProfesora->setWindowTitle(QApplication::translate("DodajProfesora", "Dialog", 0));
        label->setText(QApplication::translate("DodajProfesora", "Unesite podatke o profesoru", 0));
        label_2->setText(QApplication::translate("DodajProfesora", "Ime", 0));
        label_3->setText(QApplication::translate("DodajProfesora", "Prezime", 0));
        label_4->setText(QApplication::translate("DodajProfesora", "Email", 0));
        label_5->setText(QApplication::translate("DodajProfesora", "Username", 0));
        label_6->setText(QApplication::translate("DodajProfesora", "Password", 0));
    } // retranslateUi

};

namespace Ui {
    class DodajProfesora: public Ui_DodajProfesora {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DODAJPROFESORA_H
