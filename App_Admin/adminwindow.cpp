#include "adminwindow.h"
#include "ui_adminwindow.h"
#include "adminwindow.h"
#include "dodajpredmet.h"
#include "dodajprofesora.h"
#include <QFileDialog>
#include <QTimer>
#include <QMessageBox>
#include <QDebug>
#include <QWidget>
#include <QStandardItem>
#include <QAbstractItemModel>
#include <QAbstractItemView>

AdminWindow::AdminWindow(QWidget *parent) :
    View(parent),
    ui(new Ui::AdminWindow)
{
    ui->setupUi(this);
    ui->predmetiView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->predmetiView->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->predmetiView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    ui->profesorPredmetaView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->profesorPredmetaView->setSelectionMode(QAbstractItemView::NoSelection);
    ui->profesorPredmetaView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    ui->profesoriView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->profesoriView->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->profesoriView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    ui->predmetiProfesoraView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->predmetiProfesoraView->setSelectionMode(QAbstractItemView::NoSelection);
    ui->predmetiProfesoraView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    QIcon ikonica(":/Slike/Ikonica.ico");
    this->setWindowIcon(ikonica);
    this->setWindowTitle("Kviz Bumerang");
}

AdminWindow::~AdminWindow()
{
    delete ui;
}
void AdminWindow::osvezi_predmete()
{
    this->prikaz_Predmeta();
    //QModelIndex ql;
    //this->on_profesoriView_activated(ql);
}
void AdminWindow::osvezi_profesore()
{
    this->prikaz_Profesora();
    // int vrsta=ui->predmetiView->selectionModel()->currentIndex().row();
    // int kolona=ui->predmetiView->selectionModel()->currentIndex().column();
    // ui->predmetiView->model()->index(vrsta,kolona);
    // QModelIndex sa;
    // this->on_predmetiView_activated(sa);
}
void AdminWindow::prikaz_Predmeta()
{
    QList<Predmet> predmeti=this->controller->vrati_predmete();

    QStandardItemModel * model = new QStandardItemModel(predmeti.count(),1,this); //x Rows and 1 Column
    model->setHorizontalHeaderItem(0, new QStandardItem(QString("Naziv")));
    for(int i = 0; i < predmeti.count() ; i++)//za svaku vrstu
    {
        model->setItem(i,0,new QStandardItem(QString(predmeti[i].vrati_ime())));
    }
    ui->predmetiView->setModel(model);
    ui->profesorPredmetaView->setModel( new QStandardItemModel(0,0) );
}
void AdminWindow::prikaz_Profesora()
{
    QList<Profesor> profesori=this->controller->vrati_profesore();

    QStandardItemModel * model = new QStandardItemModel(profesori.count(),5,this); //x Rows and 5 Columns
    model->setHorizontalHeaderItem(0, new QStandardItem(QString("Ime")));
    model->setHorizontalHeaderItem(1, new QStandardItem(QString("Prezime")));
    model->setHorizontalHeaderItem(2, new QStandardItem(QString("Email")));
    model->setHorizontalHeaderItem(3, new QStandardItem(QString("Username")));
    model->setHorizontalHeaderItem(4, new QStandardItem(QString("Password")));

    for(int i = 0; i < profesori.count() ; i++)//za svaku vrstu
    {
        model->setItem(i,0,new QStandardItem(QString(profesori[i].vrati_Ime())));
        model->setItem(i,1,new QStandardItem(QString(profesori[i].vrati_Prezime())));
        model->setItem(i,2,new QStandardItem(QString(profesori[i].vrati_Email())));
        model->setItem(i,3,new QStandardItem(QString(profesori[i].vrati_Username())));
        model->setItem(i,4,new QStandardItem(QString(profesori[i].vrati_Password())));
        //QString pomocna=profesori[i].vrati_Ime()+ " " + profesori[i].vrati_Prezime()+ " "+ profesori[i].vrati_Email();
    }
    ui->profesoriView->setModel(model);
    if( ui->profesoriView->selectionModel()->selectedRows().count()!=0 ){
        QModelIndex index;
        index=ui->profesoriView->selectionModel()->selectedIndexes().at(0);
        on_profesoriView_clicked(index);
    }
    else
        ui->predmetiProfesoraView->setModel( new QStandardItemModel(0,0) );

}

void AdminWindow::on_dodajPredmetButton_clicked()
{
    dpredmeta=new DodajPredmet(this);
    dpredmeta->AddListener(this->controller);
    dpredmeta->prikaz_Profesora();
    dpredmeta->setModal(true);
    dpredmeta->show();
}
void AdminWindow::on_brisiPredmetButton_clicked()
{
    int brSelektovan=ui->predmetiView->selectionModel()->selectedRows().count();
    if(brSelektovan==0)
    {

        QMessageBox::warning(this,"Kviz Bumernag","Niste selektovali predmet za brisanje!",QMessageBox::Ok,QMessageBox::NoButton);

    }
    else
    {
        int odg=QMessageBox::question(this,"Obrisati?","Jeste li sigurni da zelite da obrisete?",
                                      QMessageBox::Ok,QMessageBox::Cancel);
        if( odg==QMessageBox::Cancel )
            return;
        int vrsta=ui->predmetiView->selectionModel()->currentIndex().row();//broj vrste
        QString ime_predmeta=ui->predmetiView->model()->data(ui->predmetiView->model()->index(vrsta,0)).toString();
        //u 0.koloni ime predmeta
        this->controller->brisi_predmet(ime_predmeta);
    }
}
//selektovao je predmet
void AdminWindow::on_predmetiView_clicked(const QModelIndex &index)
{
    int vrsta=ui->predmetiView->selectionModel()->currentIndex().row();//broj vrste
    QString ime_predmeta=ui->predmetiView->model()->data(ui->predmetiView->model()->index(vrsta,0)).toString();
    //u 0.koloni ime predmeta
    Profesor prof=this->controller->komanda_vrati_profesora_predmeta(ime_predmeta);
    QStandardItemModel * model = new QStandardItemModel(1,5,this);
    model->setHorizontalHeaderItem(0, new QStandardItem(QString("Ime")));
    model->setHorizontalHeaderItem(1, new QStandardItem(QString("Prezime")));
    model->setHorizontalHeaderItem(2, new QStandardItem(QString("Email")));
    model->setHorizontalHeaderItem(3, new QStandardItem(QString("Username")));
    model->setHorizontalHeaderItem(4, new QStandardItem(QString("Password")));
    model->setItem(0,0,new QStandardItem(QString(prof.vrati_Ime())));
    model->setItem(0,1,new QStandardItem(QString(prof.vrati_Prezime())));
    model->setItem(0,2,new QStandardItem(QString(prof.vrati_Email())));
    model->setItem(0,3,new QStandardItem(QString(prof.vrati_Username())));
    model->setItem(0,4,new QStandardItem(QString(prof.vrati_Password())));
    ui->profesorPredmetaView->setModel(model);
}

//selektovao je nekog profesora
void AdminWindow::on_profesoriView_clicked(const QModelIndex &index)
{
    int vrsta=ui->profesoriView->selectionModel()->currentIndex().row();//broj vrste
    QString email_profesora=ui->profesoriView->model()->data(ui->profesoriView->model()->index(vrsta,2)).toString();
    //u 2.koloni email profesora
    QList<Predmet> pointer=this->controller->komanda_vrati_predmete_profesora(email_profesora);
    if(pointer.count()!=0)
    {
        QStandardItemModel * model = new QStandardItemModel(pointer.count(),1,this); //x Rows and 1 Column
        model->setHorizontalHeaderItem(0, new QStandardItem(QString("Naziv")));
        for(int i = 0; i < pointer.count(); i++)//za svaku vrstu
        {
            model->setItem(i,0,new QStandardItem(QString(pointer[i].vrati_ime())));
        }
        ui->predmetiProfesoraView->setModel(model);
    }
    else{
        ui->predmetiProfesoraView->setModel( new QStandardItemModel(0,0) );
    }
}

void AdminWindow::on_dodajProfesoraButton_clicked()
{
    dprofesora=new DodajProfesora(this);
    dprofesora->AddListener(this->controller);
    dprofesora->setModal(true);
    dprofesora->show();
}

void AdminWindow::on_brisiProfesoraButton_clicked()
{
    int brSelektovan=ui->profesoriView->selectionModel()->selectedRows().count();
    if(brSelektovan==0)
    {
        QMessageBox::warning(this,"Kviz Bumernag","Niste selektovali profesora za brisanje!",QMessageBox::Ok,QMessageBox::NoButton);
    }
    else
    {
        int odg=QMessageBox::question(this,"Obrisati?","Jeste li sigurni da zelite da obrisete?",
                                      QMessageBox::Ok,QMessageBox::Cancel);
        if( odg==QMessageBox::Cancel )
            return;
        int vrsta=ui->profesoriView->selectionModel()->currentIndex().row();//broj vrste
        QString email_profesora=ui->profesoriView->model()->data(ui->profesoriView->model()->index(vrsta,2)).toString();
        //u 2.koloni email predmeta
        this->controller->brisi_profesora(email_profesora);
    }
}

void AdminWindow::odgovor_error(Error * greska)
{
    QString naziv_greska=greska->vrati_greska();
    qDebug()<<naziv_greska;
    QMessageBox::warning(this,"Kviz Bumerang",naziv_greska,QMessageBox::Ok,QMessageBox::NoButton);
    return;
}

void AdminWindow::profesor_je_logovan(QString profa)
{
    QMessageBox::warning(this,"Kviz Bumerang","Profesor logovan:"+profa,QMessageBox::Ok,QMessageBox::NoButton);
    return;
}
void AdminWindow::odgovor_diskonektovan()
{
    /* delete this->controller;
     Controller *con=new Controller();
     con->postavi_komunikaciju();

     LoginWindow w;
     w.AddListener(con);
     w.show(); */
    QMessageBox::warning(this,"Kviz Bumerang","Error - diskonektovan sa servera!",QMessageBox::Ok,QMessageBox::NoButton);
    this->close();
    this->deleteLater();
}

void AdminWindow::closeEvent(QCloseEvent *event){
    int odg=QMessageBox::question(this,"Izlazak?","Jeste li sigurni da zelite da izadjete?",QMessageBox::Ok,QMessageBox::Cancel);
    if( odg==QMessageBox::Ok )
        event->accept();
    else
        event->ignore();
}



